# UltimateHarmonyReference

## 背景

本项目的灵感来源于[UltimateAndroidReference](https://github.com/aritraroy/UltimateAndroidReference)。旨在提供一个精选的鸿蒙和OpenHarmony库、工具、开源项目、书籍、博客、教程的集合。减少大家漫无目的的搜索时间，帮助大家在学习鸿蒙的过程中少踩坑、少走弯路。

希望能够有更多的人加入到鸿蒙和OpenHarmony相关的开发活动中，共建和谐开放繁荣的OpenHarmony生态。:+1:

我们本身也开源了大量的[组件库项目](https://gitee.com/organizations/isrc_ohos/projects)，并且有配套的[图文讲解专栏](https://harmonyos.51cto.com/column/41)，欢迎大家去隔壁逛逛。


## 开源贡献规则

这个项目是开源的，欢迎大家推荐自己认为有用的、有意义的工程。

如果想要贡献资源，只需在issue中遵循以下格式：`[名称]（链接）-描述`，并说明推荐原因。

如果您有任何建议，可以随时在本项目的issue里提出。

如果有交流合作的需求也欢迎直接和我们联系：isrc_hm@iscas.ac.cn


## 目录

* [库](#库)
    * [动画](#动画)
    * [图表绘制](#图表绘制)
    * [错误报告和跟踪](#错误报告和跟踪)
    * [图像加载](#图像加载)
    * [图像处理](#图像处理)
    * [日志](#日志)
    * [网络](#网络)
    * [存储](#存储)
    * [测试](#测试)
    * [UI组件](#UI组件)
    * [实用工具](#实用工具)
    * [安全](#安全)
    * [文件数据](#文件数据)
    * [多媒体](#多媒体)
    * [其他](#其他)
* [开源HAP](#开源HAP)
* [插件](#插件)
    * [免费](#免费)
    * [免费 (+ 专业版付费)](#免费 (+ 专业版付费))
* [鸿蒙kotlin相关](#鸿蒙kotlin相关)
* [DevEco快捷键](#DevEco快捷键)
* [网站](#网站)
* [书籍](#书籍)
* [优秀文章](#优秀文章)
* [视频教程](#视频教程)
* [第三方组织](#第三方组织)


## 库
一些比较优秀的鸿蒙开源库，免去反复造轮子的麻烦，方便鸿蒙软件开发。

### 动画

* [ProgressWheel_ohos](https://gitee.com/isrc_ohos/progress-wheel_ohos) - 基于开源项目ProgressWheel的鸿蒙化移植开发，可以实现自定义环形进度条的功能，支持环形进度条的旋转、进度增加、文本设置。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/3250)、[知乎](https://zhuanlan.zhihu.com/p/349398534)、[CSDN](https://blog.csdn.net/VincenZsw/article/details/113652505)）
* [ContinuousScrollableImageView_ohos](https://gitee.com/isrc_ohos/continuous-scrollable-image-view_ohos) - 基于开源项目ContinuousScrollableImageView的鸿蒙化移植开发，可以显示具有连续滚动效果的图像，支持设置图像源、缩放类型、持续时间和方向
* [AZExplosion_ohos](待开源) - 基于开源项目AZExplosion_ohos的鸿蒙化移植开发，模仿ExplosionField的粒子爆炸效果
* [AVLoadingIndicatorView_ohos](https://gitee.com/isrc_ohos/avloading-indicator-view_ohos) - 基于开源项目AVLoadingIndicatorView 进行鸿蒙化的移植和开发，支持加载动画的开关和隐藏，支持多种加载动画效果。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/2928)、[知乎](https://zhuanlan.zhihu.com/p/346029792)、[CSDN](https://blog.csdn.net/VincenZsw/article/details/112980936)）
* [LoadingView](https://github.com/isoftstone-dev/LoadingView_HarmonyOS) - 一个可以显示加载动画的三方组件，目前支持4种风格显示。
* [confetti](https://gitee.com/openharmony-tpc/confetti) - 一个高性能，易于配置的粒子系统库，可以通过空间对任何对象集进行动画处理。您可以指定
  起始条件和物理条件（例如X和Y加速度，边界等），然后由纸屑库负责其余的工作。
* [fresco](https://gitee.com/openharmony-tpc/fresco) - Fresco是一个功能强大的系统，用于在OpenHarmony应用程序中显示图像。
* [ContinuousScrollableImageView](https://gitee.com/openharmony-tpc/ContinuousScrollableImageView) - 简单的openharmonyOS视图可有效显示具有连续滚动效果的图像。 您可以设置图像源，scaleType，持续时间和方向。 用法示例可在示例项目中找到。 
* [DraggableView](https://gitee.com/openharmony-tpc/DraggableView) - 具有旋转和倾斜/缩放效果的可拖动图像。 
* [ohos-animated-menu-items](https://gitee.com/openharmony-tpc/ohos-animated-menu-items) - xml矢量图的加载，通过openharmony的方式使用自定义Component加AnimatorValue方式替代原有的xml动画效果，效果而言是与原库完全一致的。同时增加原有自带的点击效果，并且可以通过xml传参或api调取方式设置开关与颜色。
* [ohos-Spinkit](https://gitee.com/openharmony-tpc/ohos-Spinkit) - 一个加载动画库。
* [RippleEffect](https://gitee.com/openharmony-tpc/RippleEffect) - 一种水波纹效果。
* [HorizontalPicker](https://gitee.com/openharmony-tpc/HorizontalPicker) - 一个横向滑动选择器。
* [ohos-ExpandIcon](https://gitee.com/openharmony-tpc/ohos-ExpandIcon) - 一个动态的指示箭头。
* [Leonids](https://gitee.com/openharmony-tpc/Leonids) - Leonids是可与标准openharmony UI一起使用的粒子系统库。
* [ohosViewAnimations](https://gitee.com/openharmony-tpc/ohosViewAnimations) - 文字放大视觉效果。
* [Transitions-Everywhere](https://gitee.com/openharmony-tpc/Transitions-Everywhere) - 用于在组件之间应用过渡。 它支持许多转换。 过渡定义了开始场景到结束场景之间的运动。 过渡示例包括淡入淡出，旋转，缩放等 。
* [lottie-ohos](https://gitee.com/openharmony-tpc/lottie-ohos) - 一个移动库，用于解析作为json导出的Adobe After Effects动画，并在移动设备上本地呈现它们。 
* [ohos-spruce](https://gitee.com/hihopeorg/ohos-spruce)-ohos-spruce是一个轻量级的动画库，可帮助编排屏幕上的动画。由于存在许多不同的动画库，开发人员需要确保每个视图都在适当的时间进行动画处理。


### 图表绘制

* [MPChart_ohos](https://gitee.com/isrc_ohos/mp-chart_ohos) - 基于开源项目MPAndroidChart进行鸿蒙化的移植和开发，支持多种数据图的绘制
* [HistogramComponent](https://github.com/isoftstone-dev/BarGraphView_HarmonyOS) - 可以更快速实现一个简单的柱状图功能，对外提供数据源，修改柱状图颜色和间距的接口。


### 错误报告与跟踪

* [WatchDog_ohos](https://gitee.com/isrc_ohos/anr-watch-dog-ohos) - 基于开源项目WatchDog进行鸿蒙化的移植和开发，可检测到鸿蒙系统的ANR错误并引发有意义的异常

### 图像加载

* [uCrop_ohos](https://gitee.com/isrc_ohos/u-crop_ohos) - 基于开源项目uCrop进行鸿蒙化的移植和开发，作为裁剪组件，使用鸿蒙的DataAbility实现了对鸿蒙系统相册的读写。
* [GifImage](https://github.com/isoftstone-dev/gif_HarmonyOS) - 一个可以显示加载动态图片（gif格式）的三方组件。
* [HarmonyOS ArkUI之仿微信朋友圈图片预览](https://harmonyos.51cto.com/posts/9246) - 本文介绍仿微信朋友圈实现列表展示，九宫格小图图片展示，点击图片进行图片预览，图片左右滑动切换。
* [HarmonyOS Sample 之 PixelMap 图像功能开发 ](https://harmonyos.51cto.com/posts/8976) - 本文介绍图像功能开发。


### 图像处理

* [Crop_ohos](https://gitee.com/isrc_ohos/crop_ohos) - 基于开源项目Android-crop进行鸿蒙化的移植和开发，该组件提供了一个自定义的裁剪框——可以在被裁减的图片范围内移动或缩放。
* [uCrop_ohos](https://gitee.com/isrc_ohos/u-crop_ohos) - 基于开源项目uCrop进行鸿蒙化的移植和开发，作为比较火的裁剪组件，功能强大。
* [ImageCropper_ohos](https://gitee.com/isrc_ohos/image-cropper_ohos) - 基于开源项目AndroidImageCropper进行鸿蒙化的移植和开发，裁剪组件。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/3254)、[知乎](https://zhuanlan.zhihu.com/p/347985294)、[CSDN](https://blog.csdn.net/VincenZsw/article/details/113392390)）
* [compress](https://github.com/isoftstone-dev/Compressor_Harmony) - 一个轻量级图像压缩库,允许将大照片压缩成小尺寸的照片，图像质量损失非常小。
* [RoundedImage](https://github.com/isoftstone-dev/RoundedImage_HarmonyOS) - 可以将图片显示成圆形，椭圆形，圆角矩形，目前仅支持上述三种样式显示。
* [Gilde_HarmonyOS](https://github.com/isoftstone-dev/Gilde_HarmonyOS.git) - 一款非常优秀的图片处理工具，支持多种格式图片加载,采用磁盘缓存、内存缓存方式实现预加载，指定缓存大小实现节省内存避免OOM，操作方便简单易用。
* [自定义圆形图片](https://harmonyos.51cto.com/posts/2423) - 将图片设置为圆形显示的组件。
* [glide](https://gitee.com/openharmony-tpc/glide) - Glide是一个针对openharmony的快速高效的图像加载库，专注于平滑滚动。Glide提供了易于使用的API，高性能和可扩展的资源解码管道以及自动资源池。Glide hmos testapplication +移植的代码项目Glide支持获取，解码和显示视频静止图像，图像和动画GIF。Glide包含一个灵活的api，使开发人员可以插入几乎所有网络堆栈。默认情况下，Glide使用基于自定义HttpUrlConnection的堆栈，但还包括实用程序库，这些实用程序库可插入Google的Volley项目或Square的OkHttp库。Glide的主要重点是使任何种类的图像列表尽可能平滑和快速地滚动，但是Glide在几乎所有需要获取，调整大小和显示远程图像的情况下也很有效。

### 日志

* [Timber_ohos](https://gitee.com/isrc_ohos/timber_ohos) - 基于开源项目Timber进行鸿蒙化的移植和开发，增强鸿蒙输出日志的能力。
* [Hugo](https://github.com/JakeWharton/hugo) - 可以为Annotation-triggered方法注入调用log，以供调试。
* [Logger](https://github.com/isoftstone-dev/Logger_Harmony) - log日志打印组件，输出的日志在开发工具的Hilog控制台中, 结构比较清晰可分辨。

### 网络

* [VideoCache_ohos](https://gitee.com/isrc_ohos/video-cache_ohos) - 基于开源项目AndroidVideoCache进行鸿蒙化的移植和开发，支持自动缓存视频并在断网状态下播放视频。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/3463)、[知乎](https://zhuanlan.zhihu.com/p/358484308)）
* [AsyncHttpHarmony](https://github.com/isoftstone-dev/Http-Async-HarmonyOS) - 更加高效实现网络请求及数据处理。
* [fresco](https://gitee.com/openharmony-tpc/fresco) - Fresco是一个功能强大的系统，用于在OpenHarmony应用程序中显示图像。
* [network-connection-class](https://gitee.com/openharmony-tpc/network-connection-class) - 测量移动端网络质量的组件。
* [okhttp-OkGo](https://gitee.com/openharmony-tpc/okhttp-OkGo) -  一个基于okhttp的标准RESTful风格的网络框架。
* [ThinDownloadManager](https://gitee.com/openharmony-tpc/ThinDownloadManager) - 主要用于下载文件的网络库 。
* [Fast-ohos-Networking](https://gitee.com/openharmony-tpc/Fast-ohos-Networking) - 一个功能强大的网络库，用于在 OkHttp Networking Layer 之上进行 Android 应用中的任何类型的网络连接。它负责处理连接过程中的所有操作，所以你只需要发送请求并接收响应。
* [okdownload](https://gitee.com/openharmony-tpc/okdownload) - 可靠，灵活，快速而强大的下载引擎。 
* [ohos-async-http](https://gitee.com/openharmony-tpc/ohos-async-http) - 一个网络请求框架。
* [ohosAsync](https://gitee.com/openharmony-tpc/ohosAsync) - 一个低级网络协议库。 
* [ion](https://gitee.com/openharmony-tpc/ion) - 加载并多种方式裁剪显示图片文件、加载json字符串、加载显示本地media和rawfile资源图片文件、不支持为图片加载前、加载中和加载失败添加占位图、不支持图片加载动画效果。
* [ReactiveNetwork](https://gitee.com/openharmony-tpc/ReactiveNetwork) - 用于侦听RxJava Observables的网络连接状态和Internet连接。 它是用反应式编程方法编写的。 
* [okhttputils](https://gitee.com/openharmony-tpc/okhttputils) - 一个改善的okHttp封装库。

### 存储

- [ActiveOhos_sqlite](https://github.com/isoftstone-dev/Active_HarmonyOS) - 简化了sqlite数据库的连接，并且对HarmonyOS原生的API进行封装加强，使sqlite数据库的读写更加方便。

### 测试

* [AssertJ](https://github.com/joel-costigliola/assertj-core) - AssertJ是一个库，提供了易于使用的富类型断言
* [Cucumber](https://github.com/autonomousapps/Cappuccino) - 自动编写自己的IdlingResource（作者声明）。同时可以自动关闭系统动画。
* [Hamcrest](https://github.com/hamcrest/JavaHamcrest) - Java（和原始）版本的Hamcrest
* [JUnit4](https://github.com/junit-team/junit4) - 面向程序员的Java测试框架
* [Mockito](https://github.com/mockito/mockito) - 最流行的mock框架，用于单元测试。
* [Truth](https://github.com/google/truth) - Java单元测试的断言/命题框架

### UI 组件

* [DanmakuFlameMaster_ohos](https://gitee.com/isrc_ohos/danmaku-flame-master_ohos) - 基于开源项目DanmakuFlameMaster进行鸿蒙化的移植和开发，B站的弹幕库，支持显示、隐藏、显示、暂停、继续、发送、定时发送弹幕。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/3261)、[知乎](https://zhuanlan.zhihu.com/p/341064525)、[CSDN](https://blog.csdn.net/VincenZsw/article/details/111703361)）
* [SlidingMenu_ohos](https://gitee.com/isrc_ohos/sliding-menu_ohos) - 基于开源项目SlidingMenu进行鸿蒙化的移植和开发，支持滑动菜单展示/隐藏
* [SnackBar_ohos](https://gitee.com/isrc_ohos/SnackBar_ohos) - 基于开源项目SnackBar进行鸿蒙化的移植和开发，支持显示弹出式提醒，包括文字和点击效果
* [Banner_ohos](https://gitee.com/isrc_ohos/banner_ohos) - 基于开源项目Banner进行鸿蒙化的移植和开发，鸿蒙广告图片轮播控件。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/3341)、[知乎](https://zhuanlan.zhihu.com/p/354631658)、[CSDN](https://blog.csdn.net/VincenZsw/article/details/114398112)）
* [Alerter](https://github.com/Tapadoo/Alerter) -一个定制通知视图
* [Material Dialogs](https://github.com/afollestad/material-dialogs) - 一个漂亮、流畅、可定制的对话框API
* [PrecentPositionLayout](https://github.com/isoftstone-dev/PersentPositionLayout_HarmonOS) - 一种以百分比方式定义的PrecentPositionLayout布局容器，通过它可以很方便的实现屏幕自适应。
* [xrecyclerview](https://gitee.com/openharmony-tpc/XRecyclerView) - 一个实现了下拉刷新，滚动到底部加载更多以及添加header功能的的RecyclerView。使用方式和RecyclerView完全一致，不需要额外的layout，不需要写特殊的adater。 
* [ohos-gif-drawable](https://gitee.com/openharmony-tpc/ohos-gif-drawable) -  一个对于动画GIF的“视图”和“可绘制”，实现了Animatable和 MediaPlayerControl这两个接口。 
* [TakePhoto](https://gitee.com/openharmony-tpc/TakePhoto) - 一个开源工具库，用于获取照片（拍摄照片）并在Harmony设备上压缩图像。
* [PhotoView](https://gitee.com/openharmony-tpc/PhotoView) - 一款扩展自Android ImageView ,支持通过单点/多点触摸来进行图片缩放的智能控件,功能实用和强大。
* [ReactiveNetwork](https://gitee.com/openharmony-tpc/ReactiveNetwork) -  是一个OHOS库，用于监听网络连接状态以及与RxJava Observables的Internet连接。
* [PullToZoomInListView](https://gitee.com/openharmony-tpc/PullToZoomInListView) - 一个自定义的列表视图，滑动ListView时使其HeaderView跟随滑动缩放的组件。
* [drawee_text_view](https://gitee.com/openharmony-tpc/drawee-text-view) - 一个简单的ohos组件，用于使用Fresco在Text组件周围显示图像。 
* [yoga_layout](https://gitee.com/openharmony-tpc/yoga) - 一种实现Flexbox的跨平台布局引擎。 
* [PatternLockView](https://gitee.com/openharmony-tpc/PatternLockView) - 一个自定义屏幕图案解锁控件，该库使您可以轻松，快速地在应用程序中实现模式锁定机制。它非常易于使用，并且提供了许多自定义选项，可以更改此视图的功能和外观以满足您的需求。
* [MZBannerView](https://gitee.com/openharmony-tpc/MZBannerView) - 基于仿魅族BannerView的图片轮播控件,支持多种模式切换：普通ViewPager使用，普通Banner使用，仿魅族Banner使用。
* [SlantedTextView](https://gitee.com/openharmony-tpc/SlantedTextView) - 一个倾斜的TextView,适用于标签效果。
* [GoodView](https://gitee.com/openharmony-tpc/GoodView) - 鸿蒙点赞+1效果，支持文本和图像
* [SlideAndDragListView](https://gitee.com/openharmony-tpc/SlideAndDragListView) - 一个自定义ListContaner控件，可以左右滑动菜单，上下拖拽改变item位置。
* [LayoutManagerGroup](https://gitee.com/openharmony-tpc/LayoutManagerGroup) - 一组自定义视图，负责测量和放置RecyclerView中的项目视图，并确定何时回收用户不再可见的项目视图的策略。 该库允许以对齐方式排列梯形图和特殊滑道等视图。
* [polygonsview](https://gitee.com/openharmony-tpc/polygonsview) - 模仿掌上英雄联盟能力分析效果。
* [SimpleCropView](https://gitee.com/openharmony-tpc/SimpleCropView) - 适用于鸿蒙的图像裁剪库,它简化了裁剪图像的代码，并提供了易于自定义的UI。
* [LoadSir](https://gitee.com/openharmony-tpc/LoadSir) - 一个高效易用，低碳环保，扩展性良好的加载反馈页管理框架，在加载网络或其他数据时候，根据需求切换状态页面，
  可添加自定义状态页面，如加载中，加载失败，无数据，网络超时，如占位图，登录失效等常用页面。可配合网络加载框架，结合返回状态码，错误码，数据进行状态页自动切换，封装使用效果更佳。
* [CookieBar](https://gitee.com/openharmony-tpc/CookieBar) - CookieBar是一个轻量级的库，用于在屏幕顶部或底部显示简短的消息。
* [CircleRefreshLayout](https://gitee.com/openharmony-tpc/CircleRefreshLayout) - 这是一个自定义下拉刷新控件，包含有趣的动画。
* [Alerter](https://gitee.com/openharmony-tpc/Alerter) - 支持原有的核心功能，动画没有原组件的体验好。
* [PatternLockView](https://gitee.com/openharmony-tpc/PatternLockView) - 一个自定义屏幕图案解锁控件，该库使您可以轻松，快速地在应用程序中实现模式锁定机制。它非常易于使用，并且提供了许多自定义选项，可以更改此视图的功能和外观以满足您的需求。它还支持RxJava 2视图绑定，因此，如果您喜欢响应式编程（就像我一样），则可以在用户绘制模式时获得更新流。
* [ValueCounter](https://gitee.com/openharmony-tpc/ValueCounter) - 实现组件计数器功能。
* [ImageCoverFlow](https://gitee.com/openharmony-tpc/ImageCoverFlow) - 轮播图片覆盖预览。
* [FloatingActionButton](https://gitee.com/openharmony-tpc/FloatingActionButton) - 具有许多功能的HMOS浮动动作按钮的另一种实现。
* [StateViews](https://gitee.com/openharmony-tpc/StateViews) - 创建并显示进度，数据或错误视图的简便方法。
* [FancyToast-ohos](https://gitee.com/openharmony-tpc/FancyToast-ohos) - 一个库，它将标准的android吐司带到一个新的层次，有各种样式选择。根据代码来做吐司。
* [WaveSideBar](https://gitee.com/openharmony-tpc/WaveSideBar) - 带有波浪效果的索引侧栏。
* [WaveView](https://gitee.com/openharmony-tpc/WaveView) - openHarmony的波形图，可用作进度条。
* [ohos-Bootstrap](https://gitee.com/openharmony-tpc/ohos-Bootstrap) - ohos-Bootstrap是一个openharmony库，它提供了根据 Twitter Bootstrap规范。 这可以让您花费更多时间 而不是试图在整个应用程序中获得一致的主题，尤其是在您已经熟悉Bootstrap框架的情况下。
* [NavigationTabBar](https://gitee.com/openharmony-tpc/NavigationTabBar) - 导航选项卡栏，用于与色彩缤纷的互动实现开放和谐。
* [MaterialProgressBar](https://gitee.com/openharmony-tpc/MaterialProgressBar) - MaterialProgressBar在UI上具有一致的外观。 
* [flexbox-layout](https://gitee.com/openharmony-tpc/flexbox-layout) - FlexboxLayout是一个库项目，将CSS Flexible Box Layout Module的类似功能引入了openharmony。 
* [floatingsearchview](https://gitee.com/openharmony-tpc/floatingsearchview) - 实现浮动搜索栏（也称为持久搜索）的搜索视图。 
* [michaelbel_BottomSheet](https://gitee.com/openharmony-tpc/michaelbel_BottomSheet) - BottomSheet带有材质设计概念的ohos对话库。
* [search-dialog](https://gitee.com/openharmony-tpc/search-dialog) - 带有内置搜索选项的令人敬畏且可自定义的搜索对话框。 
* [Lichenwei-Dev_ImagePicker](https://gitee.com/openharmony-tpc/Lichenwei-Dev_ImagePicker) - ImagePicker支持图片，视频单选，多选，多文件夹切换，大图片预览和自定义图片加载器等功能。 
* [Swipecards](https://gitee.com/openharmony-tpc/Swipecards) - 自定义卡片，左右飞滑删除卡片，上下滑卡片回弹复位。
* [StatefulLayout](https://gitee.com/openharmony-tpc/StatefulLayout) - 显示最常见的状态模板，如加载，清空，错误等。要做的就是用StatefulLayout包装目标区域（视图） 。
* [material-intro-screen](https://gitee.com/openharmony-tpc/material-intro-screen) - Material intro screen 的设计灵感来自于 [Material Intro](https://gitee.com/openharmony-tpc/material-intro-screen/blob/master/material-intro-screen/src/main/java/agency/tango/materialintroscreen/slice/MaterialIntroSlice.java) ， 为了使简介屏幕易于所有人使用并尽可能方便的扩展，我倾注心血几乎完全重写所有功能。
* [NumberProgressBar](https://gitee.com/openharmony-tpc/NumberProgressBar) - NumberProgressBar是各种类型的progressBar的组合。 
* [BezierMaker](https://gitee.com/openharmony-tpc/BezierMaker) - 通过de Casteljau算法绘制贝塞尔曲线，并计算它的切线，实现1-7阶贝塞尔曲线的形成动画。
* [ShowcaseView](https://gitee.com/openharmony-tpc/ShowcaseView) - ShowcaseView库旨在通过独特且吸引人的叠加层向用户突出显示和展示应用的特定部分。这个库非常适合指出用户的兴趣点，突出模糊但是有用的内容。
* [SlideSwitch](https://gitee.com/openharmony-tpc/SlideSwitch) - 一个开关，可以滑动它来打开或关闭。
* [TextDrawable](https://gitee.com/openharmony-tpc/TextDrawable) - 一个轻量级的库提供带有字母/文本的图像，例如Gmail应用程序。 它扩展了ShapeElement类，因此可以与现有/自定义/网络图像类一起使用。 还包括用于创建shapeElement的流畅接口和可自定义的ColorGenerator。 
* [SlidingLayout](https://gitee.com/openharmony-tpc/SlidingLayout) - SlidingLayout是一种Component控件，可以帮助你实现类似微信网页浏览的下拉功能。
* [ArcProgressStackView](https://gitee.com/openharmony-tpc/ArcProgressStackView) - 在弧形模式下显示进度条，并提供信息和全面控制。 
* [BGAQRCode-ohos](https://gitee.com/openharmony-tpc/BGAQRCode-ohos) - 定制各式各样的扫描框。
* [BottomBar](https://gitee.com/openharmony-tpc/BottomBar) - 底部导航栏功能。
* [CircleProgress](https://gitee.com/openharmony-tpc/CircleProgress) - 圆环形进度条。
* [TimetableView](https://gitee.com/openharmony-tpc/TimetableView) - 是一款开源的、完善、高效的课程表控件。
* [CalendarListview](https://gitee.com/openharmony-tpc/CalendarListview) - CalendarListview提供了一种日历选择日期的简便方法。
* [glide-transformations](https://gitee.com/openharmony-tpc/glide-transformations) - 一个开放谐波转换库，可为Glide提供各种图像转换。
* [BGARefreshLayout-ohos](https://gitee.com/openharmony-tpc/BGARefreshLayout-ohos) - 实现下拉刷新效果，并增加上载。
* [CustomWaterView](https://gitee.com/openharmony-tpc/CustomWaterView) - 自定义仿支付宝蚂蚁森林水滴控件，实现水滴飘浮状态，点击水滴边降落边消失，重置水滴。
* [CountdownView](https://gitee.com/openharmony-tpc/CountdownView) - 一个具有倒计时功能的View,满足大多数倒计时控件需求。
* [labelview ](https://gitee.com/openharmony-tpc/labelview) - 支持改变标签大小、方向、支持列表显示、支持在ImageView,button,Text上使用。
* [swipe](https://gitee.com/openharmony-tpc/swipe) - 实现页面滑动的监听，其中包含普通实现和rxjava实现.
* [UltraViewPager](https://gitee.com/openharmony-tpc/UltraViewPager) - UltraViewPager是PageSlider的扩展，封装了多个功能，为多页面切换提供解决方案。
* [Gloading](https://gitee.com/openharmony-tpc/Gloading) - 深度解耦Hos Hap中全局加载中、加载失败及空数据视图，为组件化改造过程中的解耦长征助力，分离全局加载状态视图的实现和使用。
* [BadgeView](https://gitee.com/openharmony-tpc/BadgeView) - 一个消息提醒小红点，可以修改红点样式。
* [Sequent](https://gitee.com/openharmony-tpc/Sequent) - Sequent是一个库，可以定期为多个视图连续启动动画。
* [Luban](https://gitee.com/openharmony-tpc/Luban) - 一个用于OHOS的图像压缩工具，其效率非常类似于WeChat Moments的效率。该模块有助于压缩png，jpg，位图和gif类型的图像。
* [LikeSinaSportProgress](https://gitee.com/openharmony-tpc/LikeSinaSportProgress) - 两者点赞数量对比进度条。
* [ViewPagerIndicator](https://gitee.com/openharmony-tpc/ViewPagerIndicator) - 与以下版本的PageSlider兼容的分页指示器小部件 openharmony以提高内容的可发现性。 
* [FlycoPageIndicator](https://gitee.com/openharmony-tpc/FlycoPageIndicator) - 页面指示器库。
* [SCViewPager](https://gitee.com/openharmony-tpc/SCViewPager) - 用于openHarmonyOS的Jazz一个简单的ViewPager扩展，提供了基于滚动的动画。 
* [Highlight](https://gitee.com/openharmony-tpc/Highlight) -一个用于app指向性功能高亮的库。
* [LoadingView](https://gitee.com/openharmony-tpc/LoadingView) - 简单的带有动画效果的加载控件。
* [CircleImageView](https://gitee.com/openharmony-tpc/CircleImageView) - 快速的圆形ImageView非常适合个人资料图像，它使用PixelMapShader创建自定义图像视图。 
* [desertplaceholder](https://gitee.com/openharmony-tpc/desertplaceholder) - 沙漠风格的动画占位符 。
* [SwipeCardView](https://gitee.com/openharmony-tpc/SwipeCardView) -  自定义卡片，左右飞滑删除卡片，上下滑卡片回弹复位。
* [TextBannerView](https://gitee.com/openharmony-tpc/TextBannerView) - 现在的绝大数APP特别是类似淘宝京东等这些大型APP都有文字轮播界面，实现循环轮播多个广告词等功能；这种控件俗称“跑马灯”，而TextBannerView已经实现了可垂直跑、可水平跑的跑马灯了。
* [MaterialBadgeTextView](https://gitee.com/openharmony-tpc/MaterialBadgeTextView) - 一个为Badge提供了改进的textview的库。Badge是带有插入数字的彩色圆圈，该圆圈显示在图标的右上角，通常在IM应用程序中显示新消息或新功能的作用。
* [AnimatedCircleLoadingView](https://gitee.com/openharmony-tpc/AnimatedCircleLoadingView) - 确定/不确定的加载视图动画。 
* [WhorlView](https://gitee.com/openharmony-tpc/WhorlView) - 一个加载View。
* [CircularFillableLoaders](https://gitee.com/openharmony-tpc/CircularFillableLoaders) - 个性化圆形进度显示。
* [SpinMenu](https://gitee.com/openharmony-tpc/SpinMenu) - 轮盘样式的 Component 菜单选择控件。
* [BubbleLayout](https://gitee.com/openharmony-tpc/BubbleLayout) - 气泡组件，具有自定义的笔触宽度和颜色，箭头大小，位置和方向。
* [AnimationEasingFunctions](https://gitee.com/openharmony-tpc/AnimationEasingFunctions) -  动画缓动功能。让动画更逼真！ 
* [BottomNavigationViewEx](https://gitee.com/openharmony-tpc/BottomNavigationViewEx) - 底部导航栏切换页面的实现。
* [DraggableView](https://gitee.com/openharmony-tpc/DraggableView) - 具有旋转和倾斜/缩放效果的可拖动图像。 
* [ohos-slidr](https://gitee.com/openharmony-tpc/ohos-slidr) - 另一个滑块/ seekbar，但有所不同。
* [MyLittleCanvas](https://gitee.com/openharmony-tpc/MyLittleCanvas) - 创建自定义组件。
* [CProgressButton](https://gitee.com/openharmony-tpc/CProgressButton) - 像iOS的圆圈进度按钮。
* [RippleEffect](https://gitee.com/openharmony-tpc/RippleEffect) - 按钮点击波纹效果。
* [HorizontalPicker](https://gitee.com/openharmony-tpc/HorizontalPicker) - 一个横向滑动选择器。
* [TriangleLabelView](https://gitee.com/openharmony-tpc/TriangleLabelView) - 一个三角标签View。
* [BottomNavigation](https://gitee.com/openharmony-tpc/BottomNavigation) - 一个底部导航栏控制器。
* [cardslib](https://gitee.com/openharmony-tpc/cardslib) - 一种在您的homs应用中使用Official Google CardView显示UI卡的简便方法。 
* [discreteSeekBar](https://gitee.com/openharmony-tpc/discreteSeekBar) - 动画气泡指示当前滑动值。
* [materialish-progress](https://gitee.com/openharmony-tpc/materialish-progress) - 一个material 风格的滚动式进度条(progress wheel)。
* [circular-progress-button](https://gitee.com/openharmony-tpc/circular-progress-button) - 实现环形进度按钮。
* [HeaderAndFooterRecyclerView](https://gitee.com/openharmony-tpc/HeaderAndFooterRecyclerView) - 一个ListContainer解决方案，它支持addHeaderView，addFooterView到ListContainer。
* [ProgressView](https://gitee.com/openharmony-tpc/ProgressView) - 一个进度视图，目前实现了带数字进度的水平进度条以及圆形进度条，圆形进度条包括三种风格：普通环形进度，内部垂直填充进度以及内部环形填充进度。
* [MaterialEditText](https://gitee.com/openharmony-tpc/MaterialEditText) - Material Design中对文本输入框的样式提供了标准,并且在AppCompat v21 中提供了Material Design的空间外观支持。
* [SlideUp-ohos ](https://gitee.com/openharmony-tpc/SlideUp-ohos) - 一个小型库，可让您向任何视图添加甜美的幻灯片效果。使用SlideUp向上，向下，向左或向右滑动视图！
* [EazeGraph](https://gitee.com/openharmony-tpc/EazeGraph) - 一个用于创建精美图表库。它的主要目标是创建一个轻量级的库，该库易于使用并且高度可定制，具有“最新”外观。
* [ahbottomnavigation](https://gitee.com/openharmony-tpc/ahbottomnavigation) - 底部导航库。
* [ButtonProgressBar](https://gitee.com/openharmony-tpc/ButtonProgressBar) - ButtonProgressBar一个下载按钮进度条。
* [DatePicker](https://gitee.com/openharmony-tpc/DatePicker) - 一个日期选择器。
* [PercentageChartView](https://gitee.com/openharmony-tpc/PercentageChartView) - 一个百分比图表，显示任何给定任务或信息的进度。
* [CountAnimationTextView](https://gitee.com/openharmony-tpc/CountAnimationTextView) - 一个很小的库使对Text的动画计数变得非常容易。
* [MultiWaveHeader](https://gitee.com/openharmony-tpc/MultiWaveHeader) - 一个可以高度定制每个波形的水波控件。
* [CircleView](https://gitee.com/openharmony-tpc/CircleView) - 包含标题和副标题的圆形视图。
* [ParallaxViewPager)](https://gitee.com/openharmony-tpc/ParallaxViewPager) - 视差背景效果。
* [MultiCardMenu](https://gitee.com/openharmony-tpc/MultiCardMenu) - 叠加菜单加载。
* [circular-music-progressbar](https://gitee.com/openharmony-tpc/circular-music-progressbar) - 此“环形进度条”是为需要精美音乐进度条的音乐播放器设计和制作的。 
* [FlycoTabLayout](https://gitee.com/openharmony-tpc/FlycoTabLayout) - 多样化导航栏。
* [WheelView](https://gitee.com/openharmony-tpc/WheelView) - 一个自定义的滚轮类控件，样式简洁。
* [ohos-HoloCircularProgressBar](https://gitee.com/openharmony-tpc/ohos-HoloCircularProgressBar) - 实现环形进度条，可以改变进度条颜色等属性。
* [sweet-alert-dialog](https://gitee.com/openharmony-tpc/sweet-alert-dialog) - 清新文艺，快意灵动的甜心弹框。
* [WheelPicker](https://gitee.com/openharmony-tpc/WheelPicker) - 简单而梦幻般的滚轮视图。
* [AvatarImageView](https://gitee.com/openharmony-tpc/AvatarImageView) - 一种在openharmony平台上创建AvatarImageView的简单方法，该平台可以显示圆形文本或圆形图像，以及SquareAvatarImageView可以用于显示圆形图像。 
* [michaelbel_BottomSheet](https://gitee.com/openharmony-tpc/michaelbel_BottomSheet) - BottomSheet带有材质设计概念的ohos对话库。
* [CircularProgressView](https://gitee.com/openharmony-tpc/CircularProgressView) - 是一个圆形进度条openharmony视图，旨在模仿ProgressBar的Material版本。
* [EasySwipeMenuLayout](https://gitee.com/openharmony-tpc/EasySwipeMenuLayout) - 滑动菜单库不仅适用于ListContainer，还适用于所有视图。 
* [ExpandableLayout](https://gitee.com/openharmony-tpc/ExpandableLayout) - 一个用于openharmony的可扩展布局容器。
* [ohos-flowlayout](https://gitee.com/openharmony-tpc/ohos-flowlayout) - 如果当前行中没有空格，则将其内容包装到下一行。 
* [ohos-viewbadger](https://gitee.com/openharmony-tpc/ohos-viewbadger) - 一个简单的文本标签视图，可以作为“徽章”应用到在运行时动态创建的任何给定组件，而不必在布局中迎合它。
* [pinned-section-listview](https://gitee.com/openharmony-tpc/pinned-section-listview) - pinnedsectionlist是易于使用的ListContainer，具有用于openharmony的固定部分。 pinnedsection是一个标题视图，该视图粘贴到列表的顶部，直到该部分的至少一项可见为止 。
* [SlideshowToolbar](https://gitee.com/openharmony-tpc/SlideshowToolbar) - 一个使用slideshowimageview的幻灯片工具栏 。
* [MaterialDesignLibrary](https://gitee.com/openharmony-tpc/MaterialDesignLibrary) - 小部件的动画特效demo库。
* [ProgressPieView](https://gitee.com/openharmony-tpc/ProgressPieView) - 用于显示高度可定制的饼图中的进度。 
* [Ratingbar](https://gitee.com/openharmony-tpc/Ratingbar) - 使用RatingBar来实现节目评分，例如反馈或其他所需的评分。 
* [StickyListHeaders](https://gitee.com/openharmony-tpc/StickyListHeaders) - 为ListContainer提供高性能的替代品。
* [MetaballLoading](https://gitee.com/openharmony-tpc/MetaballLoading) - 二维元球加载 。
* [DiscreteSlider](https://gitee.com/openharmony-tpc/DiscreteSlider) - 提供带有“材质设计”规范中所示的值标签的滑块，以及一个API。该库还为您提供范围滑块模式.
* [SwipeBack](https://gitee.com/openharmony-tpc/SwipeBack) - 可以使用手势完成活动。 您可以设置滑动方向，例如左，上，右和下。
* [material-dialogs](https://gitee.com/openharmony-tpc/material-dialogs) -  它具有几乎所有带有多个测试用例的UI组件，其中包括带有文本，图像，按钮，ListContainer项目，调色板，自定义视图和进度栏的对话框。 
* [GestureLock](https://gitee.com/openharmony-tpc/GestureLock) - 手势解锁密码。
* [ohos-SwitchView](https://gitee.com/openharmony-tpc/ohos-SwitchView) - 轻巧的开关视图风格，可实现和谐统一 。
* [bottomsheet](https://gitee.com/openharmony-tpc/bottomsheet) - 一个openharmony组件，从屏幕底部显示一个可忽略的视图。 BottomSheet可以替代对话框和菜单，但可以保留任何视图，因此用例无穷无尽。 该存储库不仅包括BottomSheet组件本身，还包括一组在底部表单中显示的通用视图组件。 这些位于commons模块中。 
* [slideview](https://gitee.com/openharmony-tpc/slideview) - 一个简单却很棒的滑动按钮，可实现和谐。 
* [MultiType](https://gitee.com/openharmony-tpc/MultiType) - MultiType提供了便捷的方法来轻松开发复杂的ListContainer。 使用此库，我们可以插入其他类型的元素，而无需更改任何旧的itemprovider代码，并使它们更具可读性。 
* [SectorProgressView](https://gitee.com/openharmony-tpc/SectorProgressView) - 一个圆形进度显示控件。
* [XEditText](https://gitee.com/openharmony-tpc/XEditText) - 带删除功能的EditText；显示或者隐藏密码；可设置自动添加分隔符分割电话号码、银行卡号等；支持禁止Emoji表情符号输入。
* [ProtractorView](https://gitee.com/openharmony-tpc/ProtractorView) - 半圆形搜寻栏视图，用于从0°到180度之间选择一个角度。 
* [williamchart](https://gitee.com/openharmony-tpc/williamchart) - Williamchart是一个开放和谐图书馆，可以在和谐应用程序中快速实现引人入胜的图表。
* [SwitchButton](https://gitee.com/openharmony-tpc/SwitchButton) - 一个美观，轻巧，易于定制的开关小部件 。
* [progressbutton](https://gitee.com/openharmony-tpc/progressbutton) - progressbutton是占位面积很小的自定义进度指示器。默认实现提供了一个pin进度按钮。
* [PageIndicatorView](https://gitee.com/openharmony-tpc/PageIndicatorView) - 是一个轻型库，用于指示PageSlider的选定页面具有不同的动画，并能够根据需要自定义它。 
* [WhorlView](https://gitee.com/openharmony-tpc/WhorlView) - 一个加载的视图。
* [RulerView](https://gitee.com/openharmony-tpc/RulerView) - 一系列卷尺控件，包含：基本卷尺控件（体重尺，或其它）、金额卷尺控件、时间卷尺控件。
* [PRDownloader](https://gitee.com/openharmony-tpc/PRDownloader) - 下载器可用于下载任何类型的文件，如图像、视频、pdf、har等。
* [ReadMoreTextView](https://gitee.com/openharmony-tpc/ReadMoreTextView) - 带有修剪文本的自定义Text。
* [Fast-ohos-Networking ](https://gitee.com/openharmony-tpc/Fast-ohos-Networking) - 一个功能强大的网络库，用于在 OkHttp Networking Layer 之上进行应用中的任何类型的网络连接。它负责处理连接过程中的所有操作，所以你只需要发送请求并接收响应。
* [LovelyDialog](https://gitee.com/openharmony-tpc/LovelyDialog) - 一组简单的对话框包装类库，旨在帮助您轻松创建精美对话框。
* [SwipeActionAdapter](https://gitee.com/openharmony-tpc/SwipeActionAdapter) - 用于ohos的类似邮箱的滑动手势库。
* [RxBus](https://gitee.com/openharmony-tpc/RxBus) - 一个事件总线，旨在使您的应用程序进行有效的通信。RxBus支持批注（@ produce / @ subscribe），它可以为您提供在其他线程（例如MAIN_THREAD，NEW_THREAD，IO，COMPUTATION，TRAMPOLINE，EXECUTOR， 单和处理程序。 RxBus还提供事件标签来定义事件。 方法的第一个（也是唯一的）参数和标记定义事件类型。 
* [BGAQRCode-ohos](https://gitee.com/openharmony-tpc/BGAQRCode-ohos) - 一个可以定制各式各样的扫描框。
* [自定义icon](https://harmonyos.51cto.com/posts/7807)-HarmonyOS JS UI 自定义icon组件

### 实用工具

* [Joda-time_ohos](https://gitee.com/isrc_ohos/joda-time_ohos) - 基于开源项目SnackBar进行鸿蒙化的移植和开发，鸿蒙日期和时间处理库。
* [Parceler_ohos](https://gitee.com/isrc_ohos/parceler_ohos) - 基于开源项目Parceler进行鸿蒙化的移植和开发，支持各种数据类型的快速序列化与反序列化，简单易用
* [SwipeCaptcha_ohos](https://gitee.com/isrc_ohos/swipe-captcha_ohos) - 基于开源项目SwipeCaptcha进行鸿蒙化的移植和开发，滑动验证码组件。此组件配套有详细的介绍和讲解（[51CTO](https://harmonyos.51cto.com/posts/3402)、[知乎](https://zhuanlan.zhihu.com/p/356619916)、[CSDN](https://blog.csdn.net/VincenZsw/article/details/114873257)）
* [ViewServer_ohos](https://gitee.com/isrc_ohos/view-server_ohos) - 基于开源项目ViewServer进行鸿蒙化的移植和开发，用于支持未来可能会推出的鸿蒙Hierarchy Viewer
* [Zbar_ohos](https://gitee.com/isrc_ohos/ZBar_ohos) - 基于开源项目Zbar进行鸿蒙化的移植和开发，条形码阅读。
* [Gson](https://github.com/google/gson) -  Java序列化/反序列化库，用于将Java对象转换为JSON并返回。
* [Guava](https://github.com/google/guava) - Google Java核心库。
* [PermissionsDispatcher](https://github.com/hotchemi/PermissionsDispatcher) - 简单的基于注释的API处理运行时权限 。
* [ProtoBuf](https://github.com/google/protobuf) - 协议缓冲区——Google的数据交换格式 。
* [UpDownfile](https://github.com/isoftstone-dev/FileDownload_HarmonyOS) - 基于Okhttp为基础进行二次封装，是一款非常好用的文件上传下载框架。
* [RxJava](https://github.com/ReactiveX/RxJava) - RxJava – JVM的反应性扩展–一个库，用于使用Java VM的可观察序列组成异步和基于事件的程序。
* [RxBus](https://gitee.com/openharmony-tpc/RxBus) - 一个事件总线，旨在使您的应用程序进行有效的通信。RxBus支持批注（@ produce / @ subscribe），它可以为您提供在其他线程（例如MAIN_THREAD，NEW_THREAD，IO，COMPUTATION，TRAMPOLINE，EXECUTOR， 单和处理程序。 RxBus还提供事件标签来定义事件。 方法的第一个（也是唯一的）参数和标记定义事件类型。 
* [BGAQRCode-ohos](https://gitee.com/openharmony-tpc/BGAQRCode-ohos) - 可定制各式各样的扫描框,可定制全屏扫描,可定制要识别的码的格式,可以控制闪光灯，方便夜间使用,zxing二维码扫描功能,ZBar 扫描条码、二维码「已解决中文乱码问题」。
* [BGAQRCode-ohos](https://gitee.com/openharmony-tpc/BGAQRCode-ohos) - 可定制各式各样的扫描框, 可定制全屏扫描,可定制要识别的码的格式,可以控制闪光灯，方便夜间使用,可以二指缩放预览,ZXing 生成可自定义颜色、带 logo 的二维码,ZXing 扫描条码、二维码,ZBar 扫描条码、二维码「已解决中文乱码问题」
* [RushOrm](https://gitee.com/openharmony-tpc/RushOrm) - RushOrm通过将Java类映射到SQL表来替代对SQL的需求。
* [xUtils3](https://gitee.com/openharmony-tpc/xUtils3) - xUtils 包含了orm, http(s), image, view注解, 特性强大, 方便扩展。
* [ohos-Universal-Image-Loader](https://gitee.com/openharmony-tpc/ohos-Universal-Image-Loader) - 现在图像加载库的伟大祖先 UIL旨在为图像加载、缓存和显示提供一个强大、灵活和高度可定制的工具。它提供了大量的配置选项和良好的控制图像加载和缓存过程。
* [CameraView](https://gitee.com/openharmony-tpc/CameraView) - 这是一个模仿微信拍照的ohos开源控件点击拍照10s的视频大概1.9M左右长按录视频（视频长度可设置）录制完视频可以浏览并且重复播放前后摄像头的切换可以设置小视频保存路径。
* [ohos-ZBLibrary](https://gitee.com/openharmony-tpc/ohos-ZBLibrary) - Ohos_ZBLibrary 架构，提供一套开发标准（View, Data, Event）以及模板和工具类并规范代码。封装层级少，简单高效兼容性好。OKHttp 网络请求、Glide 图片加载、ZXing 二维码、自动缓存以及各种 Base、Demo、UI、Util 直接用。
* [RxBinding](https://gitee.com/openharmony-tpc/RxBinding) -用于OpenHarmonyOS的UI小部件的RxJava绑定API。
* [Anadea_RxBus](https://gitee.com/openharmony-tpc/Anadea_RxBus) - 基于RxJava并针对OpenHarmony进行了优化的事件总线。
* [rx-preferences](https://gitee.com/openharmony-tpc/rx-preferences) - SharedPreference对OpenHarmony有反应。
* [XXPermissions](https://gitee.com/openharmony-tpc/XXPermissions) - XxPermissions权限请求框架 一键式权限请求框架。
* [ohosAutoLayout](https://gitee.com/openharmony-tpc/ohosAutoLayout) - 屏幕适配方案，直接填写设计图上的像素尺寸即可完成适配。
* [ReLinker](https://gitee.com/openharmony-tpc/ReLinker) - 用于openharmony OS的强大的本机库加载器,ReLinker通过将标准System.loadLibrary调用替换为更可靠的实现来解决这些问题,在cpp文件的帮助下实现JNI级别的代码。
* [vlayout](https://gitee.com/openharmony-tpc/vlayout) - Project vlayout是ListContainer的功能强大的LayoutManager扩展，它为ListContainer提供了一组布局。当网格，列表和其他布局位于同一listContainer中时，使其能够处理复杂的情况。通过为ListContainer提供自定义的LayoutManager，VirtualLayout可以在单个视图上优雅地布局具有不同样式的子视图。定制的LayoutManager管理一系列的layoutHelpers，其中每个控件都为特定的位置范围项实现特定的布局逻辑。顺便说一下，还支持实现我们的自定义layoutHelper并将其提供给框架。
* [PersistentCookieJar](https://gitee.com/openharmony-tpc/PersistentCookieJar) - 基于SharedPreferences的OkHttp3的持久性CookieJar实现。该库通常用于存储从http网址获取的cookie。再次，如果我们点击url并获取cookie，那么在保存之前将检查cookie是否过期。如果它过期了，它将清除以前的并保存新的。
* [LoganSquare](https://gitee.com/openharmony-tpc/LoganSquare) - 适用于ohos的最快的JSON解析和序列化库。LoganSquare基于Jackson的流API，能够始终胜过GSON和Jackson的Databind库400％或更多1。通过依靠编译时注释处理来生成代码，您知道JSON的解析和序列化速度比任何其他可用方法都快。通过使用此库，您将能够利用Jackson的流API的功能，而不必编写tedius（涉及JsonParsers或JsonGenerators的底层代码）的代码。相反，只需将模型对象标记为a @JsonObject，将字段注释为@JsonFields，我们将为您完成繁重的工作。
* [ohos-ZBLibrary](https://gitee.com/openharmony-tpc/ohos-ZBLibrary) - Ohos_ZBLibrary 架构，提供一套开发标准（View, Data, Event）以及模板和工具类并规范代码。封装层级少，简单高效兼容性好。OKHttp 网络请求、Glide 图片加载、ZXing 二维码、自动缓存以及各种 Base、Demo、UI、Util 直接用。
* [AutoDispose](https://gitee.com/openharmony-tpc/AutoDispose) - AutoDispose是一个RxJava工具库，用于通过处置/取消将RxJava流的执行自动绑定到提供的作用域。它有助于自动处理基于事件的Observable以避免内存泄漏。
* [RxLifeCycle](https://gitee.com/openharmony-tpc/RxLifeCycle) - 该库使我们能够根据第二个生命周期流自动完成序列。此功能在openharmony中很有用，因为不完整的订阅可能会导致内存泄漏。
* [DividerDrawable](https://gitee.com/openharmony-tpc/DividerDrawable) - 该库可以帮助您轻松地在现有视图上布局和绘制分隔线。为了获得更好的性能，请不要再为绘图分隔线创建新视图，而应使用drawable。
* [fresco](https://gitee.com/openharmony-tpc/fresco) - Fresco是一个功能强大的系统，用于在OpenHarmony应用程序中显示图像。壁画负责图像的加载和显示，因此您不必这样做。它将从网络，本地存储或本地资源加载图像，并显示一个占位符，直到图像到达为止。它具有两个级别的缓存；一个在内存中，另一个在内部存储器中。
* [ohosMP3Recorder](https://gitee.com/openharmony-tpc/ohosMP3Recorder) - 以捕捉到来自你的计算机音频MP3，麦克风的声音，互联网流媒体，Winamp的播放，Windows媒体播放器音乐音频文件，可以录制你喜欢的章节。
* [webp-ohos](https://gitee.com/openharmony-tpc/webp-ohos) - webp是Chrome的webp解码器的改编版，并添加了JNI包装器，可轻松在Java代码中使用它。
* [Rxohos](https://gitee.com/openharmony-tpc/Rxohos) - Rxohos：RxJava3的openharmony特定绑定的反应性扩展。该模块向RxJava添加了最小的类，这些类使在openharmony应用程序中编写反应式组件变得容易且轻松。更具体地说，它提供了一个可在主线程或任何给定EventRunner上进行调度的Scheduler。
* [butterknife](https://gitee.com/openharmony-tpc/butterknife) - openharmony组件的字段和方法绑定，它使用注释处理为您生成样板代码。
* [logger](https://gitee.com/openharmony-tpc/logger) - 记录器：简单，漂亮，功能强大的记录器
* [stefanjauker_BadgeView](https://gitee.com/openharmony-tpc/stefanjauker_BadgeView) - BadgeView openharmony项目，模仿iOS Springboard'徽章'的扩展TextView。它可以覆盖在任何其他项目上。
* [TinyPinyin](https://gitee.com/openharmony-tpc/TinyPinyin) - 快速、低内存占用的汉字转拼音库。
* [Compressor](https://gitee.com/openharmony-tpc/Compressor) - Compressor是一个轻量级且功能强大的和谐图像压缩库。通过Compressor，您可以将大照片压缩为较小尺寸的照片，而图像质量的损失则很小或可以忽略不计。不支持WebP。
* [CompressHelper](https://gitee.com/openharmony-tpc/CompressHelper) - 图片压缩，压缩Pixelmap，CompressImage 主要通过尺寸压缩和质量压缩，以达到清晰度最优。
* [xLog](https://gitee.com/openharmony-tpc/xLog) - 简单、美观、强大、可扩展的 openHormony 和 Java 日志库，可同时在多个通道打印日志，如 hilog、Console 和文件。如果你愿意，甚至可以打印到远程服务器（或其他任何地方）。
* [DragScaleCircleView](https://gitee.com/openharmony-tpc/DragScaleCircleView) - 一个可拖动与缩放的自定义圆形View，它其实是一个ImageView，自带了拖放圆形区域的功能，可以用于从一张图片截取一个圆形的视图。
* [MultiType](https://gitee.com/openharmony-tpc/MultiType) - MultiType：为ListContainer创建多个类型更容易，更灵活。以前，当我们需要开发复杂的ListContainer时，这是困难且麻烦的工作。一旦需要添加新的项目类型，就必须转到原始适配器文件并仔细修改一些旧代码，这些适配器类将变得更加复杂。MultiType提供了便捷的方法来轻松开发复杂的ListContainer。使用此库，我们可以插入其他类型的元素，而无需更改任何旧的itemprovider代码，并使它们更具可读性。
* [FlycoRoundView](https://gitee.com/openharmony-tpc/FlycoRoundView) - 库可帮助openharmony内置视图轻松方便地设置圆形矩形背景，并因此可以减少相关的形状资源。
* [BadgeView](https://gitee.com/openharmony-tpc/BadgeView) - BadgeView是个消息提醒小红点，可以修改红点样式。
* [CustomActivityOnCrash](https://gitee.com/openharmony-tpc/CustomActivityOnCrash) - CustomActivityOnCrash：CustomActivityOnCrash是OHOS库，允许应用崩溃时启动自定义活动，而不是显示讨厌的“不幸的是，X已停止”对话框。
* [ticker](https://gitee.com/openharmony-tpc/ticker) - ticker是一个简单的openharmony组件，用于显示滚动文本。此行情自动收录器可处理字符串之间的平滑动画以及调整字符串的大小。
* [ohos-validation-komensky](https://gitee.com/openharmony-tpc/ohos-validation-komensky) - ohos-validation-komensky：一个简单的库，用于使用注释来验证表单中的用户输入。
* [ormlite-ohos](https://gitee.com/openharmony-tpc/ormlite-ohos) - 该软件包提供了特定于ohos的功能。（ormlite-core-与ormlite-jdbc或ormlite-ohos一起提供lite Java ORM的核心ORMLite功能）。
* [TheMVP](https://gitee.com/openharmony-tpc/TheMVP) - 在MVP模式下，View和Model是完全分离没有任何直接关联的(比如你在View层中完全不需要导Model的包，也不应该去关联它们)。使用MVP模式能够更方便的帮助Ability(或AbiltySlice)职责分离，减小类体积，使项目结构更加清晰。
* [RxScreenshotDetector](https://gitee.com/openharmony-tpc/RxScreenshotDetector) - 带有DataAbilityHelper和Rx的openharmony屏幕截图检测器。
* [seismic](https://gitee.com/openharmony-tpc/seismic) - openharmony设备抖动检测。它会侦听并识别具有适当硬件的设备上的抖动。
* [SystemBarTint](https://gitee.com/openharmony-tpc/SystemBarTint) - 在openharmony系统UI上应用背景着色“色调”样式非常适合基于壁纸的活动，例如主屏幕启动器，但是提供的最小背景保护使其在其他类型的活动中不那么有用，除非您在布局中提供自己的背景。确定给定设备配置的系统UI的大小，位置和存在可能并非易事。该库提供了一种使用颜色值为系统栏创建背景“色调”的简单方法。
* [lock-screen](https://gitee.com/openharmony-tpc/lock-screen) -  简单漂亮的锁屏库可设置检查密码。使用锁屏库轻松保护您的应用程序，就像启动意图一样简单。
* [EventBus](https://gitee.com/openharmony-tpc/EventBus) - 是openhormony和Java的发布/订阅事件总线。
* [okhttputils](https://gitee.com/openharmony-tpc/okhttputils) -  对okhttp的封装类。
* [ohos-NoSql](https://gitee.com/openharmony-tpc/ohos-NoSql) - 轻巧，结构简单的NoSQL数据库，可实现Openharmony。
* [Matisse](https://gitee.com/openharmony-tpc/Matisse) -  Matisse是为Openharmony设计的精心设计的本地图像和视频选择器。你可以Ability或AbilitySlice中使用它，选择包括JPEG，PNG，GIF的图像以及包括MPEG，MP4的视频，应用不同的主题，包括两个内置主题和自定义主题，不同的图像加载器，定义自定义过滤规则。
* [cropper](https://gitee.com/openharmony-tpc/cropper) - 裁剪器是一种图像裁剪工具。它提供了一种以XML方式和以编程方式设置图像的方法，并在图像顶部显示了可调整大小的裁剪窗口。然后，调用方法getCroppedImage（）将返回由裁剪窗口标记的PixelMap。
* [ImagePicker](https://gitee.com/openharmony-tpc/ImagePicker) - 自定义相册，完全仿微信UI，实现了拍照、图片选择（单选/多选）、 裁剪 、旋转、等功能。
* [FileDownloader](https://gitee.com/openharmony-tpc/FileDownloader) - HarmonyOpenSource多任务文件下载引擎。
* [Keyframes](https://gitee.com/openharmony-tpc/Keyframes) - 关键帧是（1）从After Effects文件中提取图像动画数据的ExtendScript脚本和（2）OpenHarmony的相应渲染库的组合。关键帧可用于导出和渲染高质量，基于矢量的动画，这些动画具有复杂的形状和路径曲线，而且文件占用空间极小。
* [device-year-class](https://gitee.com/openharmony-tpc/device-year-class) - Device Year Class是一个Openharmony库，它实现了一个简单的算法，该算法将设备的RAM，CPU内核和时钟速度映射到那些规格组合被认为是高端的年份。这使开发人员可以根据电话硬件的功能轻松修改应用程序的行为。
* [Toasty](https://gitee.com/openharmony-tpc/Toasty) - 主要将底层安卓接口调用的实现修改成鸿蒙接口的实现，将三方库鸿蒙化，供开发鸿蒙应用的开发者使用。
* [LitePal](https://gitee.com/openharmony-tpc/LitePal) - LitePal是一个开源的openharmony库，它使开发人员可以非常轻松地使用SQLite数据库。您无需编写SQL语句即可完成大多数数据库操作，包括创建或升级表，crud操作，聚合函数等。LitePal的设置也非常简单，您可以在不到5个的时间内将其集成到项目中分钟。
* [preferencebinder](https://gitee.com/openharmony-tpc/preferencebinder) - 使用注释处理，此库使加载SharedPreferences值和侦听更改变得容易。
* [Rajawali](https://gitee.com/archermind-ti/rajawali)- Rajawali是HarmonyOS基于OpenGL ES 1.X/2.0/3.0的3D引擎库。提供了丰富的示例，开发者使用它可以很方便地进行3D应用开发。
* [AAChartCore](https://gitee.com/chinasoft3_ohos/AAChartCore)- AAChartCore是一组易于使用、极其精美而又强大的数据可视化图表框架。
* [RxImagePicker](https://gitee.com/chinasoft5_ohos/RxImagePicker)- RxImagePicker一个灵活可高度定制的图片选择架构，提供了读取相册和调用系统相机拍照的功能。用户可自由选择图片加载框架，还可设置主题图片的选择界面和预览图片的自适应界面。
* [HarmonyOS Sample 之 Pasteboard 分布式粘贴板 ](https://harmonyos.51cto.com/posts/9216)- HarmonyOS提供系统剪贴板服务的操作接口，支持用户程序从系统剪贴板中读取、写入和查询剪贴板数据，以及添加、移除系统剪贴板数据变化的回调。
* [鸿蒙开源第三方组件——SwipeCaptcha_ohos3.0旋转验证组件 ](https://harmonyos.51cto.com/posts/9260)- 基于安卓平台的滑动拼## 二级标题图验证组件。
* [#星光计划1.0# HarmonyOS 自定义组件之仿微信朋友圈主页 ](https://harmonyos.51cto.com/posts/9064)- 自定义一个仿微信朋友圈主页的组件。
* [#星光计划1.0# 鸿蒙开源第三方组件——crop_image_layout_ohos ](https://harmonyos.51cto.com/posts/9016)- crop_image_layout_ohos组件能对图片进行旋转和自定义裁切的操作，并且无论待裁切图片原尺寸有多大或多小，最终都将在以最佳尺寸在组件内显示。同时，该组件操作界面简洁且使用方法简单，易被开发者使用或优化，能够提升应用的丰富性和可操作性。
* [DistributedVideoPlayer 分布式视频播放器(二) ](https://harmonyos.51cto.com/posts/8821)- 视频播放器。
* [DistributedVideoPlayer 分布式视频播放器(一) ](https://harmonyos.51cto.com/posts/8816)- 视频播放器。
* [鸿蒙开源第三方组件——SwipeCaptcha_ohos2.0滑动拼图验证组件 ](https://harmonyos.51cto.com/posts/8787)- 滑动拼图验证组件。
* [纯JS分布式视频播放应用 ](https://harmonyos.51cto.com/posts/8745)- 视频播放应用。

### 安全

- [PermissionsDispatcher](https://gitee.com/openharmony-tpc/PermissionsDispatcher) - PermissionsDispatcher提供了一个简单的基于注释的API来处理运行时权限。该库减轻了编写一堆检查语句（无论是否已授予您权限）带来的负担，以保持您的代码干净安全。
- [Dexter](https://gitee.com/openharmony-tpc/Dexter) - 一个可简化运行时的权限请求过程。 允许用户在运行应用程序时授予或拒绝权限，而不是在安装应用程序时授予所有权限。

### 文件数据

- [hawk](https://gitee.com/openharmony-tpc/hawk) -  Hawk，用于OpenHarmonyOS的安全，简单的键值存储。
- [ohos-NoSql](https://gitee.com/openharmony-tpc/ohos-NoSql) - 轻巧，结构简单的NoSQL数据库，可实现Openharmony。
- [Parceler](https://gitee.com/openharmony-tpc/Parceler) -  Parceler：简单捆绑数据注入框架。
- [LitePal](https://gitee.com/openharmony-tpc/LitePal) - LitePal是一个开源的openharmony库，它使开发人员可以非常轻松地使用SQLite数据库。您无需编写SQL语句即可完成大多数数据库操作，包括创建或升级表，crud操作，聚合函数等。LitePal的设置也非常简单，您可以在不到5个的时间内将其集成到项目中分钟。
- [tray](https://gitee.com/openharmony-tpc/tray) -  托盘是此提到的*显式跨进程数据管理方法*。托盘还提供了高级API，通过升级和迁移机制，它可以非常轻松地访问和维护您的数据。
- [ohos-database-sqlcipher](https://gitee.com/openharmony-tpc/ohos-database-sqlcipher) - ohos-database-sqlcipher用于完全数据库加密。
- [ormlite-ohos](https://gitee.com/openharmony-tpc/ormlite-ohos) -  该软件包提供了特定于ohos的功能。ormlite-core-与ormlite-jdbc或ormlite-ohos一起提供lite Java ORM的核心ORMLite功能。
- [FileDownloader](https://gitee.com/openharmony-tpc/FileDownloader) - HarmonyOpenSource多任务文件下载引擎。

### 多媒体

- [CameraView](https://gitee.com/openharmony-tpc/CameraView) - CameraView这是一个模仿微信拍照的ohos开源控件

  点击拍照10s的视频大概1.9M左右长按录视频（视频长度可设置）录制完视频可以浏览并且重复播放前后摄像头的切换可以设置小视频保存路径。

- [FastBle](https://gitee.com/openharmony-tpc/FastBle) -  Ohos Bluetooth Low Energy 蓝牙快速开发框架。鸿蒙，使用简单的方式进行搜索、连接、读写等一系列蓝牙操作，并实时地得到操作反馈。

- [ohosMP3Recorder](https://gitee.com/openharmony-tpc/ohosMP3Recorder) - 以捕捉到来自你的计算机音频MP3，麦克风的声音，互联网流媒体，Winamp的播放，Windows媒体播放器音乐音频文件，可以录制你喜欢的章节。

- [webp-ohos](https://gitee.com/openharmony-tpc/webp-ohos) - webp是Chrome的webp解码器的改编版，并添加了JNI包装器，可轻松在Java代码中使用它。

- [PloyFun](https://gitee.com/openharmony-tpc/PloyFun) - 此仓库是为了提升国内下载速度的镜像仓库。

- [libyuv](https://gitee.com/openharmony-tpc/libyuv) -  libyuv是Google的开源库，用于在YUV和RGB之间进行转换，旋转和缩放。它支持在Windows，Linux，Mac和其他平台，x86，x64，arm架构以及SIMD指令加速（例如SSE，AVX，NEON）上进行编译和执行。

- [Matisse](https://gitee.com/openharmony-tpc/Matisse) -  Matisse是为Openharmony设计的精心设计的本地图像和视频选择器。你可以Ability或AbilitySlice中使用它，选择包括JPEG，PNG，GIF的图像以及包括MPEG，MP4的视频，应用不同的主题，包括两个内置主题和自定义主题，不同的图像加载器，定义自定义过滤规则。

- [ImagePicker](https://gitee.com/openharmony-tpc/ImagePicker) - 自定义相册，完全仿微信UI，实现了拍照、图片选择（单选/多选）、 裁剪 、旋转、等功能。

### 其他

- [agera](https://gitee.com/openharmony-tpc/agera) - 一组类和接口，可帮助编写功能性，异步和反应性应用程序。
- [preferencebinder](https://gitee.com/openharmony-tpc/preferencebinder) - 一个首选绑定库。
- [JAVA自定义组件](https://harmonyos.51cto.com/posts/7931) - 自定义组件有三种：基本组件的组合组件、继承基本组件、自我绘制组件。前面两种比较简单，这篇文章主要讲的是自我绘制组件


## 开源HAP

鸿蒙社区中有一些（未来会变得很多）开源HAP可供开发者学习，这个列表可以帮助你找到真正值得你花时间并且可以帮助你学到东西的开源HAP。

* [OpenHarmony Samples](https://gitee.com/openharmony/app_samples) - 为帮助开发者快速熟悉HarmonyOS和OpenHarmony SDK所提供的API和应用开发流程，提供了一系列的应用示例，即Sample。

* [官方CodeLabs](https://gitee.com/openharmony/codelabs) - 该Codelabs旨在向开发人员展示如何通过趣味场景来展示如何使用HarmonyOS能力的示例应用程序文档教程。

* [HarmonicaPreviewer](https://gitee.com/hiharmonica/harmonica-previewer) - 配套HarmonyOS北向IDE DevEco使用的HAP预览器，可以对HarmonyOS北向开发的最大程度预览，可以确保真实的界面效果展示，可以让北向开发者在没有实体硬件的情况下进行应用开发和适配，且不需要利用云端进行调试。

* [Harmony_AWS_IoT](https://gitee.com/harmonyhub/harmony-aws-iot?_from=gitee_search) - 支持鸿蒙系统的aws-iot软件包

* [Twins Piano 孪生钢琴](https://gitee.com/hiharmonica/piano) -利用鸿蒙的分布式能力实现双手双机演奏，解决单机显示键盘数目较少的缺陷，分布式开发demo


* [HarmonyHttpClient](https://gitee.com/huanghaibin_dev/harmony-http-client?_from=gitee_search#post-json-请求构建) - 鸿蒙上使用的Http网络框架

* [Harmony_AWS_IoT](https://gitee.com/harmonyhub/harmony-aws-iot?_from=gitee_search) - 支持鸿蒙系统的aws-iot软件包，基于aws iot 3.1.2 版本 

* [harmony_onenet](https://gitee.com/qidiyun/harmony_onenet?_from=gitee_search) - 支持鸿蒙系统的OneNET接入协议实现

* [0基础开发抖音App](https://harmonyos.51cto.com/posts/2095)

* [2048小游戏(腕表)](https://harmonyos.51cto.com/posts/1566)

* [鸿蒙——贪吃蛇项目](https://harmonyos.51cto.com/resource/568)

* [俄罗斯方块](https://harmonyos.51cto.com/resource/216) 

* [两个手机控制两辆智能小车比赛](https://harmonyos.51cto.com/resource/343) 

* [鸿蒙卡片小游戏：暴打七夕青蛙](https://mp.weixin.qq.com/s/vIDh0T_AL6OXm2qnRg9ysw)

* [阻击“德尔塔”：鸿蒙防疫一码通卡片](https://mp.weixin.qq.com/s/bsBdR7gVVK_-UwAaGZ3oRg)

* [鸿蒙“微信炸屎”功能实现......](https://mp.weixin.qq.com/s/ef4U9Hn-LdL2Ns7CzW4S8w)

* [一个非常实用的鸿蒙监测组件！](https://mp.weixin.qq.com/s/KqDStHWkdudQGb5XdWRSMQ)

* [在鸿蒙开发版上玩“太空人避障”游戏！](https://mp.weixin.qq.com/s/rDESsEelt_9zKvnQGIe14g)

* [鸿蒙“吃豆豆”小游戏来啦！！！](https://mp.weixin.qq.com/s/D5efIuY7sZYGXNlX618eTQ)

* [在鸿蒙上实现抖音点赞效果！](https://mp.weixin.qq.com/s/q4NlJjOcRhw-1dP9mY2GNw)

* [在鸿蒙开发板上播放《蜜雪冰城》](https://mp.weixin.qq.com/s/VJ-HGTb-Ku4UnFX-nt5jeg)

* [我为B站添加鸿蒙服务卡片！](https://mp.weixin.qq.com/s/SPNG8Xl-duXEKwLaaf0S3Q)

* [#星光计划1.0# HarmonyOS 分布式之聊天室应用](https://harmonyos.51cto.com/posts/9213)-之前给大家介绍过[【#星光计划1.0# HarmonyOS 分布式之仿抖音应用】](https://harmonyos.51cto.com/posts/8855)，此次给大家介绍一下基于鸿蒙**分布式数据服务**开发的聊天室应用，模拟现实中的聊天室对话，可以与小伙伴们互动、分享自己的故事给小伙伴。

* [#星光计划1.0# HarmonyOS 基于LYEVK-3861开发火焰报警系统](https://harmonyos.51cto.com/posts/9083)-本文内容主要讲述基于LYEVK-3861物联网开发板套件的火焰传感器，开发一个具有火焰感应报警功能的HarmonyOS应用，主要实现蓝牙设备扫描，连接，检测火焰，设置报警阈值。

* [在鸿蒙系统上实现权限请求框架——桃夭权限请求框架](https://harmonyos.51cto.com/posts/9081)-**桃夭**是鸿蒙系统上的一款权限请求框架，对请求权限的代码进行高度封装，极大的简化了申请权限的代码逻辑，同时支持在`Ability`、`FractionAbility`、`AbilitySlice`、`Fractiion`里面申请权限。

* [#星光计划1.0# HarmonyOS 分布式之仿抖音应用](https://harmonyos.51cto.com/posts/8855)-使用Java UI开发分布式仿抖音应用，上下滑动切换视频，评论功能，设备迁移功能：记录播放的视频页和进度、评论数据。

* [#星光计划1.0# 播放器流转迁移,带评论功能](https://harmonyos.51cto.com/posts/8749)-1轮播功能  2 评论功能  ,3流转功能  4视频播放功能。

## DevEco插件

在DevEco中配置好合适的插件，可以有效地提升你的鸿蒙开发工作效率。这个列表提供了经过我们挑选的DevEco插件，希望会对你的开发工作产生帮助。

### 免费版

* [Buck](https://github.com/facebook/buck) - 快速构建系统，鼓励通过各种平台和语言创建小型可重用的模块
* [Carbon](https://github.com/dawnlabs/carbon) - 创建并共享源代码的精美图片 
* [CodeGlance](https://plugins.jetbrains.com/plugin/7275-codeglance) - 将类似于Sublime中的代码的微型地图嵌入到编辑器窗格中。 使用您自定义的颜色对亮色和深色主题进行语法突出显示 
* [Codota](https://www.codota.com/ide-plugin) - 通过直接在IDE中获取出色的代码示例来增强开发工作流程。 IntelliJ的Codota插件可让您快速找到每个API类或方法的最佳代码示例 
* [Dagger IntelliJ Plugin](https://github.com/square/dagger-intellij-plugin) - 用于Dagger的IntelliJ IDEA插件，可深入了解如何使用注入和提供程序 
* [Detekt](https://github.com/arturbosch/detekt) - Kotlin的静态代码分析 
* [DTO Generator](https://plugins.jetbrains.com/plugin/7834-dto-generator) - 数据传输对象（DTO）生成器，可从给定的提要中生成Java类 
* [Infer](https://github.com/facebook/infer) - 用于Java，C，C ++和Objective-C的静态分析器 
* [JSONSchema2POJO](http://www.jsonschema2pojo.org) - 简单快速地将JSON转换为Java对象的方法 
* [Material Palette](https://www.materialpalette.com) - 快速轻松地生成Material Design调色板 
* [Methods Count](http://www.methodscount.com) - 您的完美契合APK解决方案
* [Sonar](https://github.com/facebook/Sonar) -适用于移动开发人员的桌面调试平台 
* [SonarLint](https://plugins.jetbrains.com/plugin/7973-sonarlint) - SonarLint是IntelliJ IDEA插件，可向开发人员提供有关注入Java的新错误和质量问题的动态反馈。 
* [String Manipulation](https://plugins.jetbrains.com/plugin/2162-string-manipulation) - 提供文本操作的动作，例如切换，大写，编码/解码等 

### 免费版 (+ 付费专业版)

* [Crowdin](https://crowdin.com) - 更好，更快，更智能地翻译和管理流程 
* [GitKraken](https://www.gitkraken.com) - 适用于Windows，Mac和Linux的最受欢迎的Git GUI 



## 鸿蒙kotlin相关

鸿蒙也是可以支持kotlin编程的，所以这一部分内容我们也在筹划中。未来将在此列举更多的优秀的Kotlin资源，供大家使用。

* [Spek](https://github.com/spekframework/spek) - Kotlin的规范框架 



## DevEco快捷键

DevEco的快捷键可以让开发工作流程快速流畅。让使用快捷键成为一种习惯，不要用鼠标来执行大多数操作。以下提供了一些最有用的快捷键。

*(目前仅提供Windows版本DevEco的快捷键，Mac或其它版本的快捷键会在未来加入)*

**Note：将以下大部分快捷键中<kbd>Ctrl</kbd>换成<kbd>Command</kbd>就是Mac版的相同快捷键**

### 最常用的快捷键:

| 功能                                             |                      快捷键                       |
| :----------------------------------------------- | :-----------------------------------------------: |
| 搜索当前Project中的一切 (文件、类、变量、字符等) |        <kbd>Shift</kbd> + <kbd>Shift</kbd>        |
| 搜索当前Project中的class                         |          <kbd>Ctrl</kbd> + <kbd>N</kbd>           |
| 搜索当前Project中所有的文本                      | <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F</kbd> |
| 搜索当前Project中的file                          | <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>N</kbd> |
| 搜索当前Project中的action                        | <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>A</kbd> |
| 在当前file中搜索字符                             |          <kbd>Ctrl</kbd> + <kbd>F</kbd>           |
| 显示最近在IDE中打开的file                        |          <kbd>Ctrl</kbd> + <kbd>E</kbd>           |
| 运行（可以是hap或test）                          |          <kbd>Ctrl</kbd> + <kbd>R</kbd>           |
| 在类、方法、变量的定义与引用之间跳转             |      <kbd>Ctrl</kbd> + <kbd>Left Click</kbd>      |
| 注释/取消注释所有选定行（也适用于xml文件）       |          <kbd>Ctrl</kbd> + <kbd>/</kbd>           |


### 常用的快捷键：

| 功能                                                                           | 快捷键           |
|:----------------------------------------------------------------------------------------|:---------------------:|
| 向上/向下移动一段代码                            | <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>Up/Down</kbd>  |
| 打开DevEco的说明文档                                   | <kbd>F1</kbd>                    |
| 显示当前方法所需的参数类型（光标需在方法名或参数括号内） | <kbd>Ctrl</kbd> + <kbd>P</kbd>             |
| 格式化代码以匹配DevEco的默认样式     | <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>L</kbd> |
| 删除光标后的第一个字符串                                      | <kbd>Ctrl</kbd> + <kbd>Delete</kbd>     |


### 有用的快捷键

| 功能                                 |                        快捷键                         |
| :----------------------------------- | :---------------------------------------------------: |
| 将光标移动到下一个编写检查错误位置   |                     <kbd>F2</kbd>                     |
| 转到光标处变量/方法/类的声明         |            <kbd>Ctrl</kbd> + <kbd>B</kbd>             |
| 复制当前行并粘贴在下一行             |            <kbd>Ctrl</kbd> + <kbd>D</kbd>             |
| 在弹出窗口中显示类结构               |           <kbd>Ctrl</kbd> + <kbd>F12</kbd>            |
| 在弹出窗口中列出最近编辑的文件       |   <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>E</kbd>   |
| 【Git相关】从Git远程仓pull           |            <kbd>Ctrl</kbd> + <kbd>T</kbd>             |
| 【Git相关】Commit当前变更            |            <kbd>Ctrl</kbd> + <kbd>K</kbd>             |
| 【Git相关】push到Git远程仓           |   <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>K</kbd>   |
| 折叠/展开所有代码块                  |  <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>+/-</kbd>  |
| 折叠/展开单个代码块                  |           <kbd>Ctrl</kbd> + <kbd>+/-</kbd>            |
| 在当前光标位置下方插入新行           |          <kbd>Shift</kbd> + <kbd>Enter</kbd>          |
| 完整语句（添加大括号、圆括号、分号） | <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>Enter</kbd> |



## 网站

提供一个网站列表，在上面可以找到最新的鸿蒙相关技术、新闻。

* [掘金_HarmonyOS](https://juejin.cn/tag/%E9%B8%BF%E8%92%99OS) - 掘金的HarmonyOS板块
* [51CTO_HarmonyOS](https://harmonyos.51cto.com/) - 51CTO的HarmonyOS技术社区
* [华为Developers](https://developer.huawei.com/consumer/cn/forum/) - 华为官方的技术论坛
* [电子发烧友_HarmonyOS技术社区](https://bbs.elecfans.com/harmonyos) - 电子发烧友论坛的HarmonyOS技术社区



## 优秀文章

觉得直接在网站上找资料如大海捞针？没关系，我们已经为你精心挑选了一些干货满满的文章，相信你看完之后一定会有所收获。

* [OpenAtom 教育资源仓](https://gitee.com/openatom-university/openharmony-oer) - 大量的官方收入的教育资源集合
* [安卓VS鸿蒙第三方件切换宝典 V1.0](https://harmonyos.51cto.com/posts/3260)
- [鸿蒙内核源码分析](https://my.oschina.net/weharmony?tab=newest&catalogId=7082609)
- [Feature Ability和AbilitySlice的关系](https://harmonyos.51cto.com/posts/2130)
- [使用绘图组件Canvas绘制心率曲线图](https://harmonyos.51cto.com/posts/2265)
- [多组示例演示三个样式的组合用法](https://harmonyos.51cto.com/posts/2266)
- [列表组件ListContainer](https://harmonyos.51cto.com/posts/2168)
- [Linux下的Hi3861一站式鸿蒙开发烧录（附工具）](https://harmonyos.51cto.com/posts/3333)
- [全球首发—鸿蒙开源平台OpenGL](https://harmonyos.51cto.com/posts/3127)
- [鸿蒙操作系统的前世今生](https://harmonyos.51cto.com/posts/5260)
- [鸿蒙服务卡片，为什么这么香？](https://mp.weixin.qq.com/s/Mfr0uj4jOALmY0HkEfDRpA)
- [我为B站添加鸿蒙服务卡片！](https://mp.weixin.qq.com/s/SPNG8Xl-duXEKwLaaf0S3Q)
- [HarmonyOS基于分布式的点餐系统](https://mp.weixin.qq.com/s/hewdWCZ6bZ7MbM-exVZtLA)
- [鸿蒙首款地图服务卡片，我心动了！](https://mp.weixin.qq.com/s/C_0Sc0MpnPKjvDanSKHnxA)
- [鸿蒙运动饮食卡片，消灭亚健康！](https://mp.weixin.qq.com/s/gqQlNGMXaX9kfQsIvxXRKw)
- [一文吃透鸿蒙操作系统！](https://mp.weixin.qq.com/s/-xymNvX4nzva5TYJb49-nA)
- [在鸿蒙上开发Unity游戏的方法！](https://mp.weixin.qq.com/s/vQ4tgraYw8DFAtf8pURnwg)
- [假如中国空间站用上鸿蒙系统...](https://mp.weixin.qq.com/s/kFJf2euUGkiu_4Y0x7IyGA)
- [全网首发：鸿蒙开源第三方件yoga](https://mp.weixin.qq.com/s/0csBHUluN-Dt6f5t_riqlw)
- [鸿蒙OS中实现页面跳转方法汇总！](https://mp.weixin.qq.com/s/8lupKS1iJlRUIq5I9_kPvQ)
- [短短3天！攻克鸿蒙最新物联网模组](https://mp.weixin.qq.com/s/d_bTx5U6YxdRHAcDS5yUtQ)
- [鸿蒙分布式小车游戏，可联机对战！](https://mp.weixin.qq.com/s/rypQN5w-qkyhVa4q0jxqTg)
- [一时技痒，撸了个鸿蒙购物应用！](https://mp.weixin.qq.com/s/_He5RJ5YHV9vkJ9Qb5-BLg)
- [好家伙！鸿蒙系统的编译流程](https://mp.weixin.qq.com/s/ylCT4UCqvoyOM_XKLua1Ng)
- [鸿蒙徽章组件，开发指南！](https://mp.weixin.qq.com/s/Gms7-dEaYwOFBothyre0AQ)
- [牛批！用Python远程控制交通信号灯](https://mp.weixin.qq.com/s/sSC6u1n9CRYMLPbyKm2IgQ)
- [徒手撸了一个鸿蒙文件上传下载框架！](https://mp.weixin.qq.com/s/7JmMy_U4DN_Tdr-k27AT3A)
- [鸿蒙OS中JS与Java如何进行混合开发？](https://mp.weixin.qq.com/s/vcXKJ-tBPwMAu2P9y7rorg)
- [鸿蒙版Retrofit，久等了！](https://mp.weixin.qq.com/s/SId8xpuwDWBWG825O5u9PA)
- [在鸿蒙上做一款独一无二的遥控器](https://mp.weixin.qq.com/s/e2tuEpZz5N7G25ygyeptow)
- [如何在鸿蒙系统中实现自动登录？](https://mp.weixin.qq.com/s/jt-WBsQMJGHKQ1tre7ZDdw)
- [吐血总结：鸿蒙软总线跨设备访问怎么玩？](https://mp.weixin.qq.com/s/IWF5v-zUyUmNoMFcRnC_OA)
- [【软通动力】安卓VS鸿蒙第三方件切换宝典 V1.0](https://harmonyos.51cto.com/posts/3260)
- [全体注意！一大波鸿蒙三方库已经到来！](https://harmonyos.51cto.com/posts/4039)
- [HarmonyOS | 北向开发 | 自学资源整理-鸿蒙HarmonyOS技术社区](https://harmonyos.51cto.com/posts/5357)
- [鸿蒙系统入门教程 HiSpark Wi-Fi IoT开发套件 开箱入门指导](https://harmonyos.51cto.com/posts/1234)
- [#2020征文-开发板# 用鸿蒙开发AI应用（一）硬件篇](https://harmonyos.51cto.com/posts/2647)
- [嵌入式开发小白跟连老师学鸿蒙设备开发之hello world](https://harmonyos.51cto.com/posts/3167)
- [第1~2章 鸿蒙系统介绍 和 编译环境搭建 持续更新](https://harmonyos.51cto.com/posts/943)
- [【开发板试用报告】从零了解Hi3861的硬件部分!](https://harmonyos.51cto.com/posts/1410)
- [在CentOS中安装鸿蒙LiteOS编译环境-海思Hi3861](https://harmonyos.51cto.com/posts/1268)
- [Day2：Ubuntu20.04搭建 Hispark HarmonyOS编译环境步骤整理](https://harmonyos.51cto.com/posts/2899)
- [《HarmonyOS入门宝典》全网首发免费下载](https://harmonyos.51cto.com/posts/1214)
- [鸿蒙构建系统——gn官方FAQ翻译，以及gn官方文档分享](https://harmonyos.51cto.com/posts/1889)
- [基于鸿蒙系统 + Hi3861 的wifi小车，可以通过电脑、手机控制](https://harmonyos.51cto.com/posts/1459)
- [HarmonyOS中实现页面跳转的方法汇总](https://harmonyos.51cto.com/posts/4378)
- [鸿蒙系统的编译流程及分析v1.0](https://harmonyos.51cto.com/posts/4070)
- [HarmonyOS 应用开发尝鲜——开发第一个鸿蒙应用](https://harmonyos.51cto.com/posts/1852)
- [HarmonyOS源码目录结构的理解](https://harmonyos.51cto.com/posts/2284)
- [基于Neptune开发板的键盘蓝牙模块DIY指南](https://harmonyos.51cto.com/posts/4260)
- [#2020征文-其它#鸿蒙 “JS 小程序” 数据绑定原理详解](https://harmonyos.51cto.com/posts/2311)
- [全网最全面的鸿蒙资源查询手册——UltimateHarmonyReference](https://harmonyos.51cto.com/posts/7758)
- [鸿蒙开源第三方组件——页面滑动组件 ViewPagerIndicator_ohos](https://harmonyos.51cto.com/posts/7883)
- [鸿蒙开源第三方组件——序列化与反序列化封装组件Parceler_ohos](https://harmonyos.51cto.com/posts/8066)
- [鸿蒙开源第三方组件——ANR异常监测组件 ANR-WatchDog-ohos](https://harmonyos.51cto.com/posts/7479)
- [鸿蒙开源第三方组件——自定义流式布局组件FlowLayout_ohos](https://harmonyos.51cto.com/posts/7371)
- [中科院软件所成员力作！《鸿蒙操作系统应用开发实践》正式上市！](https://harmonyos.51cto.com/posts/7346)
- [鸿蒙开源第三方组件——粒子破碎效果组件Azexplosion_ohos](https://harmonyos.51cto.com/posts/7270)
- [鸿蒙开源第三方组件——消息弹框组件SnackBar-ohos ](https://harmonyos.51cto.com/posts/6931)
- [鸿蒙开源第三方组件——日志工具组件Timber_ohos](https://harmonyos.51cto.com/posts/6492)
- [鸿蒙开源第三方组件——日期和时间处理组件JodaTime-ohos](https://harmonyos.51cto.com/posts/6075)
- [鸿蒙开源第三方组件——自定义图片缩放组件PinchImageView-ohos ](https://harmonyos.51cto.com/posts/5524)
- [鸿蒙操作系统的前世今生](https://harmonyos.51cto.com/posts/5260)
- [鸿蒙开源第三方组件——连续滚动图像组件](https://harmonyos.51cto.com/posts/4205)
- [鸿蒙开源第三方组件——MPChart_ohos 图表绘制组件](https://harmonyos.51cto.com/posts/4002)
- [鸿蒙开源第三方组件——SlidingMenu_ohos侧滑菜单组件](https://harmonyos.51cto.com/posts/3910)
- [鸿蒙开源第三方组件——uCrop_ohos图片裁剪组件](https://harmonyos.51cto.com/posts/3773)
- [鸿蒙开源第三方组件——Zbar_ohos条形码阅读器](https://harmonyos.51cto.com/posts/3574)
- [鸿蒙开源第三方组件——VideoCache_ohos视频缓存组件](https://harmonyos.51cto.com/posts/3463)
- [鸿蒙开源第三方组件——SwipeCaptcha_ohos滑动拼图验证组件](https://harmonyos.51cto.com/posts/3402)
- [鸿蒙开源第三方件组件——轮播组件Banner](https://harmonyos.51cto.com/posts/3341)
- [鸿蒙开源第三方组件 ——B站开源弹幕库引擎的迁移（下）](https://harmonyos.51cto.com/posts/3262)
- [鸿蒙开源第三方组件 ——B站开源弹幕库引擎的迁移（上）](https://harmonyos.51cto.com/posts/3261)
- [鸿蒙开源第三方组件——图片裁剪组件ImageCropper_ohos](https://harmonyos.51cto.com/posts/3254)
- [鸿蒙开源第三方组件——进度轮ProgressWheel](https://harmonyos.51cto.com/posts/3250)
- [全球首发—鸿蒙开源平台OpenGL](https://harmonyos.51cto.com/posts/3127)
- [鸿蒙开源第三方组件的迁移——加载动画库](https://harmonyos.51cto.com/posts/2928)
- [鸿蒙的驱动子系统-4-驱动配置文件的分析](https://harmonyos.51cto.com/posts/7992)
- [【木棉花】ImageCoverFlow开源组件助力七夕](https://harmonyos.51cto.com/posts/7777)
- [木棉花：【资料汇集】服务卡片相关学习资料的汇总](https://harmonyos.51cto.com/posts/7703)
- [和Android对比，HarmonyOS有什么优势？](https://harmonyos.51cto.com/posts/7488)
- [HarmonyOS原子化服务原理和架构分析](https://harmonyos.51cto.com/posts/7194)
- [鸿蒙开源组件已达700+，更实用了！](https://mp.weixin.qq.com/s/T0OqBznB9oGF1iS7fY999g)
- [鸿蒙软总线，超详细教程来啦！](https://mp.weixin.qq.com/s/ZhQD_7sTedfDnoK8MeigJw)
- [一文搞懂鸿蒙OS应用的目录结构](https://mp.weixin.qq.com/s/_Aeq2IfDgjj8ZfFgltno-Q)
- [1个简单案例讲解：鸿蒙分布式任务调度](https://mp.weixin.qq.com/s/IjkDdC6o7SM7BC7FRwUwmw)
- [鸿蒙OS通知新玩法！](https://mp.weixin.qq.com/s/w-qn8bnSA_hOcf-aPWUmTQ)
- [【木棉花】：JS模板里添加音频/视频](https://harmonyos.51cto.com/posts/7802)
- [Lifecycle感知宿主的生命周期](https://harmonyos.51cto.com/posts/8086)
- [【软通动力】安卓VSHarmonyOS三方件切换宝典 V2.0（第二部分](https://harmonyos.51cto.com/posts/5267)
- [HarmonyOS 原子化服务的商业价值与构建思路](https://harmonyos.51cto.com/posts/7882)
- [HarmonyOS基础技术赋能之对象关系映射数据库的使用](https://harmonyos.51cto.com/posts/8029)
- [打造一款定制化鸿蒙发行版](https://harmonyos.51cto.com/posts/9088)
- [HarmonyOS Sample 之 Pasteboard 分布式粘贴板 ](https://harmonyos.51cto.com/posts/9216)
- [#星光计划1.0# HarmonyOS集成HMS Core服务--小白入坑操作(2) ](https://harmonyos.51cto.com/posts/9216)
- [Hi3518鸿蒙增加自定义应用 ](https://harmonyos.51cto.com/posts/9203)
- [#星光计划1.0# OpenHarmony 源码解析之分布式任务调度（一）](https://harmonyos.51cto.com/posts/9159)
- [【木棉花】轻松玩转平行视界（上） ](https://harmonyos.51cto.com/posts/9148)
- [【木棉花】轻松玩转平行视界（下）](https://harmonyos.51cto.com/posts/9165)
- [#星光计划1.0# OpenHarmony 源码解析之Sensor子系统（上）](https://harmonyos.51cto.com/posts/9108)
- [【独家】鸿蒙开发板资料汇总（厂商资料+代码仓+教程及购买渠道）](https://harmonyos.51cto.com/posts/9109)
- [#星光计划1.0# DevEco Studio 本地模拟器上线了，快来体验吧~](https://harmonyos.51cto.com/posts/9042)
- [#星光计划1.0#【木棉花】：ETS版的数字华容道](https://harmonyos.51cto.com/posts/9012)
- [#星光计划1.0# HarmonyOS 流转之跨端迁移](https://harmonyos.51cto.com/posts/9013)
- [基于HarmonyOS ArkUI 3.0 框架，我成功开发了流式布局网络语](https://harmonyos.51cto.com/posts/9002)
- [#星光计划1.0# HarmonyOS JS FA调用PA新方式 ](https://harmonyos.51cto.com/posts/8961)
- [Hi3518鸿蒙2.0源码hb编译过程](https://harmonyos.51cto.com/posts/8946)
- [#星光计划1.0# v55.04 鸿蒙内核源码分析(重定位)  ](https://harmonyos.51cto.com/posts/8891)
- [#星光计划1.0#HarmonyOS 自定义View之图文标题 ](https://harmonyos.51cto.com/posts/8886)
- [OpenHarmony LiteOS-A内核文档之学习--系统调用 ](https://harmonyos.51cto.com/posts/8876)
- [鸿蒙轻内核M核源码分析系列十八 Fault异常处理  ](https://harmonyos.51cto.com/posts/8831)
- [#星光计划1.0# 如何在其他手机安装自己开发的鸿蒙应用 ](https://harmonyos.51cto.com/posts/8802)
- [HarmonyOS学习路之开发篇—Java UI框架(基础组件说明【三】)](https://harmonyos.51cto.com/posts/8766)
- [#星光计划1.0# 鸿蒙应用打包签名需要用到的4个文件生成方法 ](https://harmonyos.51cto.com/posts/8722)

## 书籍

提供一些鸿蒙开发相关的书籍，帮助你系统地学习鸿蒙开发。

* [鸿蒙操作系统应用开发实践](https://item.jd.com/13336048.html) - 陈美汝、郑森文、武延军、吴敬征 - 该书针对HarmonyOS SDK 4版本，对鸿蒙操作系统的应用开发基础进行了梳理和介绍并同时，构建了一个在分布式场景非常常用的视频流直播实例作为实战的内容演练，进行了详细的分析和讲解。书籍包含11章教学课件，50多个实例源代码，200多分钟视频讲解，进一步帮助读者掌握核心知识点。
* [鸿蒙应用开发实战](https://item.jd.com/13066138.html) - 张荣超 - 第一本华为操作系统HarmonyOS开发图书，以JavaScript为开发语言，示例以华为智能手表为运行载体，提供完整源代码，详解国产鸿蒙操作系统。
* [鸿蒙操作系统开发入门经典](https://item.jd.com/12879967.html) - 徐礼文 - 全面介绍华为鸿蒙操作系统实战开发，涵盖鸿蒙北向应用开发内容和南向硬件开发基础内容，包含14章教学课件和30多个源代码，助力读者快速掌握鸿蒙应用开发的技巧。
* [鸿蒙征途：App开发实战](https://item.jd.com/10034598867344.html) - 李宁 - 一本基于Java的鸿蒙操作系统（HarmonyOS）App开发指南。全书系统、由浅入深地介绍了HarmonyOS App开发的知识、相关经验和技巧，以理论与实战相结合的方式向读者呈现HarmonyOS App开发的整个过程。
* [鸿蒙应用程序开发](https://item.jd.com/13358220.html) - 董昱 - 以基础知识和实例相结合的方式，成体系地介绍鸿蒙应用程序开发的常用技术，定位为鸿蒙应用程序开发的入门图书，成体系地介绍鸿蒙应用开发的基础知识。
* [HarmonyOS IoT设备开发实战](https://item.jd.com/13288938.html) - 江苏润和软件股份有限公司 - 主要介绍如何使用HarmonyOS（鸿蒙操作系统）开发物联网设备端软件，具体包括外设控制、网络编程、物联网平台接入等。书中的实例程序均在HiSpark Wi-Fi IoT开发套件上进行测试和演示，部分章节内容也适用于其他支持HarmonyOS的物联网设备。
* [《深入浅出OpenHarmony——架构、内核、驱动及应用开发全栈》](https://item.jd.com/12926119.html)- 李传钊-本书从OpenHarmony操作系统的设计目标与设计思路开始，由表及里、深入浅出地讲解了OpenHarmony操作系统的架构、内核、驱动及应用开发基础与实战的全栈技术内容。在实战部分，从开发环境的安装配置开始，分别讲解了南向开发、北向开发的基本过程及实战案例。本书的英文版已在翻译之中，华为已将其作为面向全球推荐的OpenHarmony操作系统的官方技术教程之一。
* [《华为LiteOS：快速上手物联网应用开发》](https://item.jd.com/13377044.html)-朱有鹏，樊心昊，左新戈，涂小平-本书是第 一本关于物联网操作系统华为LiteOS的书，接入并开启鸿蒙（Harmony）时代。快速掌握华为LiteOS，在华为物联网体系赋能下更快速更低成本地开发自身的物联网产品。
- [《鸿蒙第三方组件库应用开发实战》](https://item.jd.com/13518886.html)-武延军、郑森文、朱伟、吴敬征-本书是第 一本书通过多个精选的开源组件库，详尽地讲解了如何在鸿蒙操作系统下使用这些组件库实现快捷的应用开发。同时，本书详细剖析了鸿蒙操作系统组件库的实现原理，并通过一个综合应用实战帮助读者学习更加深入的应用开发知识和技巧。
- [鸿蒙生态:开启万物互联的智慧新时代](http://product.dangdang.com/11031921178.html)-李洋-本书尝试从科普、专业与工具三个方面介绍鸿蒙。

## 视频教程

以下列表包含了一些鸿蒙开发的视频教程，从理论到实践全方面地学习鸿蒙开发。

- 北向开发
  - [HarmonyOS 2.0开发入门](https://edu.51cto.com/course/25069.html?hm) - 将对HarmonyOS 2.0的应用开发有所理解，快速入门
  - [HarmonyOS(鸿蒙)App项目实战（1）手表篇](https://edu.51cto.com/course/25054.html?hm) - 边做边学，学完之后就可以做出一个完整的HarmonyOS（鸿蒙）运动手表上的 
         App
  - [HarmonyOS(鸿蒙)应用程序开发教程教你实现多时区电子时钟](https://edu.51cto.com/course/25274.html?hm) - 以Java语言为基础介绍HarmonyOS基础知识以及从零开始开发 
         多时区电子时钟
  - [精讲鸿蒙应用程序开发](https://edu.51cto.com/course/26138.html?hm) -  帮助热爱鸿蒙的广大开发者快速实现鸿蒙应用程序开发。
  - [HarmonyOS工作原理](https://edu.51cto.com/course/25049.html?hm) - 从技术角度讲解鸿蒙系统的架构框图、鸿蒙APP的开发和部署方式等。并详细讲了鸿蒙的关键技术创新 
         与鸿蒙的生态伙伴、生态发展策略。
  - [ HarmonyOS实现无云端家人协同记账](https://edu.51cto.com/course/26517.html?hm) - 通过不需要云侧在不同手机上实现家人协同记账的案例，理解HarmonyOS分布式存储 
         能力。
  - [多设备共享涂鸦画板的鸿蒙实现方式](https://edu.51cto.com/course/26587.html?hm) - 教你从容易入手的JS开始，了解HarmonyOS开发和多终端共享、分布式软总线开发，实 
         现一个基于鸿蒙OS的多设备共享涂鸦画板。
  - [从零开发鸿蒙小游戏App](https://edu.51cto.com/course/25751.html?hm) - 从零开始，对编写的每一行代码进行讲解，带领开发者开发一款运行在鸿蒙设备上的、曾经风靡全 
         球的小游戏。
  - [基于分布式软总线技术的跨设备应用交互分享](https://edu.51cto.com/course/27974.html?hm) - 帮助开发者整体了解HarmonyOS特性及应用项目基础知识、迁移在分布式设备 
         中的应用和分布式数据库特点。
  - [分布式数据管理与传统方式的异同](https://edu.51cto.com/course/27290.html?hm) -帮助开发者了解分布式数据库、文件系统的初步应用，以及和传统的数据库、本地文件系 
         统之间的区别。
  - [HarmonyOS新特性-服务卡片应用实战演练](https://edu.51cto.com/course/28739.html?hm) - 深度了解HarmonyOS新特性服务卡片，包括使用的场景和给开发者的一些启发。 
         从而在理论技术层面和实战层面得到一定的提高。
  - [OpenHarmony平台C++开发应用](https://edu.51cto.com/course/28626.html?hm) - 教你轻鸿蒙C++如何开发、如何开放接口给JS、标准鸿蒙如何用C++开发JS组件、鸿蒙系统中 
         如何使用C++三方库以及基于标准鸿蒙系统的开发板进展情况分享。
  - [HarmonyOS应用开发系列课（基础篇）](https://developer.huaweiuniversity.com/portal/courses/HuaweiX+CBGHWDCN098/about?source=hwdev) -  华为官方视频教程。主要介绍HarmonyOS整体架构和理念、关键技术（分布式关键技术/安全和隐私/UX）、应用程序框架、以及开放能力和工具平台。
  - [HarmonyOS应用开发系列课（进阶篇）](https://developer.huaweiuniversity.com/portal/courses/HuaweiX+CBGHWDCN103/about?source=hwdev) -  华为官方视频教程。主要介绍HarmonyOS应用程序框架，HarmonyOS分布式软总线，任务调度，分布式数据管理、安全和隐私设和UX体验设计等内容。
  - [鸿蒙0S 2.0 最全面教学](https://www.bilibili.com/video/BV1uT4y1w7DZ?p=1) -  慕课网官方视频。深度对比HarmonyOS和Android架构，帮助开发者快速掌握HarmonyOS开发必备基础，并有实战项目演练帮助进阶。
  - [【全球首套鸿蒙2.0教程】鸿蒙（HarmonyOS）2.0入门与实战【李宁】](https://www.bilibili.com/video/BV1mv411179B/?spm_id_from=333.788.videocard.0) -  帮助热爱鸿蒙的广大开发者快速实现鸿蒙应用程序开发。
  - [【迄今为止，最好的鸿蒙教程】鸿蒙（HarmonyOS）应用开发入门与实践](https://www.bilibili.com/video/BV1dT4y1w7Rq?from=search&seid=8482852456609692256) -  主要讲解了鸿蒙应用开发的基本开发方法，up主自身具有10年以上的教学及开发经验，相信你一定能听懂。
  - [鸿蒙版手机开发](https://www.bilibili.com/video/BV1Th411f71F/?spm_id_from=333.788.recommend_more_video.7) -  鸿蒙系统系列课的第一门入门课，清晰、详细地讲解把鸿蒙系统的应用开发入门知识和技术点。
  - [鸿蒙系统应用开发基础入门课程](https://www.bilibili.com/video/BV1Vf4y1D7Xo?from=search&seid=10341988940474235336) -  鸿蒙系统应用开发入门课程。
  - [钊哥科普：什么是分布式操作系统？终端分布式操作系统与服务器分布式操作系统有什么区别](https://www.bilibili.com/video/BV11E411w7BS) - 讲解什么是分布式操作系统并有哪些特征，终端分布式操作系统与服务器分布式操作系统有什么区别和不同的设计思路，鸿蒙是如何实现分布式的，鸿蒙的分布式软总线、分布式数据库、分布式文件系统分别是怎样的，鸿蒙分布式可能会带来哪些前所未有应用。
  - [【鸿蒙学院】鸿蒙（HarmonyOS）必学编程语言：JavaScript开发与实战](https://www.bilibili.com/video/BV1Bf4y1D7QP/?spm_id_from=333.788.recommend_more_video.16) - 鸿蒙App可以使用JavaScript语言进行开发，学习JavaScript语言是学习鸿蒙App开发的前提。
  - [华为鸿蒙团队用63小时讲完的Java！整整400集，拿走不谢，学不会退出IT界！](https://www.bilibili.com/video/BV1Qq4y1u7kq?from=search&seid=1634327347293240003&spm_id_from=333.337.0.0) - 针对初学者。
  - [鸿蒙UI组件开发指导](https://www.bilibili.com/video/BV1Gg411T7LQ?from=search&seid=14473196236202168837&spm_id_from=333.337.0.0) - 此视频将带你了解鸿蒙UI组件。



- 南向开发
  - [Harmony鸿蒙内核Liteos-a开发](https://edu.51cto.com/course/25146.html?hm) - 对鸿蒙内核Liteos-a有一个全面的理解
  - [鸿蒙内核liteos-a移植_基于IMX6ULL](https://edu.51cto.com/course/25951.html?hm) - 帮助开发者掌握基本的Liteos-a移植技能，为更多的鸿蒙系统开发夯实基础。
  - [HarmonyOS南向驱动调试（gpio以及wifi）及烧录方法总结](https://edu.51cto.com/course/25159.html?hm) - 讲解鸿蒙南向驱动框架，包括常用的gpio以及wifi驱动的调 
          试，另外对各种烧录方法进行了总结和比较，让开发者在烧录程序的时候更加得心应手。
  - [【线下沙龙】HarmonyOS内核Liteos-a与Linux在驱动程序上的异同](https://edu.51cto.com/course/26054.html?hm) -教你如何快速学习鸿蒙liteos-a内核，并通过linux来 
          类比liteos-a，讲解在linux上的经验对学习liteos-a是否有用处、如何把这个新的内核用在自己的板子上、以及移植完成后如何验证它支持鸿蒙的各种子系统。
  - [HarmonyOS驱动框架调试总结](https://edu.51cto.com/course/26096.html?hm) -深入理解HarmonyOS驱动框架、用户态APP和内核态驱动之间的通信机制。
  - [鸿蒙系统2.0之实战讲解，深入鸿蒙内核解密运行原理](https://www.bilibili.com/video/BV1Ag411G7P9?from=search&seid=1950093366322173706) -将Android开发与鸿蒙 
          开发全方位对比，详解鸿蒙编译流程，鸿蒙系统的组件、线程和UI实战性，并分析鸿蒙运行机制源码。
  - [全网最透彻！鸿蒙OS微内核高能技术流分析，小白也能懂！](https://www.bilibili.com/video/BV1T54y1G72t?from=search&seid=8969189083905199407) -关于鸿蒙系统微 
          内核架构的优劣势介绍及原理介绍，旨在让开发者认识到什么是鸿蒙系统以及什么是微内核。
  - [钊哥科普：鸿蒙公开课 第四讲 鸿蒙的内核](https://www.bilibili.com/video/BV11E411w7BS) -讲解什么是宏内核和微内核，区别在哪里，Lite OS和TEE分别是什么。





## 相关组织

如果您喜欢关注鸿蒙和OpenHarmony相关的最新新闻，了解系统的最新技术进展，了解在鸿蒙系统上的最新的优秀应用，那么关注这个列表准没错。这个列表挑选了一些目前在鸿蒙以及OpenHarmony生态圈中相当活跃的一些第三方组织。

- ISRC_OHOS:
  -  [Gitee](https://gitee.com/isrc_ohos) - 开源大量实用的鸿蒙第三方库
  -  [51CTO](https://harmonyos.51cto.com/column/41)、[知乎](https://www.zhihu.com/people/2016yao-nu-li-zuo-zhi-xue-ba) 、[CSDN](https://blog.csdn.net/vincenzsw/category_10667674.html) - 提供详细的鸿蒙第三方库分析与使用说明，干货满满。
- 深鸿会:
  
  - [Gitee](https://gitee.com/hiharmonica) - 提供鸿蒙开发者所需要的知识分享，经验传递，项目交流平台。
- 51CTOHarmonyOS技术社区专栏:
  - [51CTO](https://harmonyos.51cto.com/column) - 大量的鸿蒙开发经验分享专栏，涉及各个方面，综合性较强。
- 软通动力HarmonyOS学院:
  - [Github](https://github.com/isoftstone-dev) - 开源大量实用的鸿蒙第三方库。
  -  [51CTO](https://harmonyos.51cto.com/column/30) - 提供详细的鸿蒙第三方库分析与使用说明。

- HarmonyHub：
  - [Gitee](https://gitee.com/harmonyhub) -  提供鸿蒙操作系统开源组件库，讲解如何使用。

- scriptiot：
  - [Gitee](https://gitee.com/scriptiot) -  针对鸿蒙OS进行深度研究，提供EVM虚拟机在Harmony OS上的适配。

- hihopeorg:
  - [Gitee](https://gitee.com/hihopeorg_group) -  为HarmonyOS提供tftp server，以及物联网应用开发知识。

## OpenHarmony JS组件开发指南
[OpenHarmony JS Demo开发讲解](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%20Demo%E5%BC%80%E5%8F%91%E8%AE%B2%E8%A7%A3.md) - 讲解如何基于OpenHarmony开发JS Demo，包括项目结构、JS FA、常用组件及自定义组件。  
  
[OpenHarmony JS项目开发流程](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%E9%A1%B9%E7%9B%AE%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md) - 讲解基于DevEco开发OpenHarmony JS应用的基本流程，包括环境配置、工程导入、证书配置、推送运行等。
